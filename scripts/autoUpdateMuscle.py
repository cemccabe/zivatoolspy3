import pymel as pm
import maya.cmds as cmds
from collections import OrderedDict
import zBuilder.builders.ziva as zva
import zBuilder.zMaya as mz
import maya.mel as mm
import zivaTools as zTool
import omUtil as omu
import meshes




def getMaterial(node):
    nodes     = cmds.ls(node, dag=True, s=True)
    shadeEng  = cmds.listConnections(nodes, type="shadingEngine")
    material  = cmds.ls(cmds.listConnections(shadeEng), materials=True)
    return material


def replaceMuscle(muscleDirPath, offFrame, onFrame, dampPadding):
    # Delete materials in obj directory before Maya can even attempt to list obj's
    [cmds.sysFile(muscleDirPath + "\\" + mat, delete=True) for mat in cmds.getFileList(folder=muscleDirPath, filespec='*.mtl')]

    filesList = []
    # Verify Obj's exist in muscleDirPath
    if cmds.getFileList(folder=muscleDirPath, filespec='*.obj') != None:
        filesList = cmds.getFileList(folder=muscleDirPath, filespec='*.obj')
    else:
        raise IndexError('No Objects (*.obj) found in specified directory')


    # Filter only zTissues
    tissueList = []
    if cmds.ls(sl=True):
        selLst = cmds.ls(sl=1)
        [tissueList.append(tissue) for tissue in cmds.ls(sl=True) if mm.eval('zQuery -t "zTissue" {0}'.format(tissue))]


    # Filter only zBones
    boneList = []
    if cmds.ls(sl=True):
        selLst = cmds.ls(sl=1)
        [boneList.append(bone) for bone in cmds.ls(sl=True) if mm.eval('zQuery -t "zBone" {0}'.format(bone))]


    if tissueList == [] and boneList == []:
        raise IndexError('No zBones or zTissue selected for update')

    if tissueList!=[]:
        # Verify each selected tissue has corresponding .obj file to update to
        missingLst = []
        for t in tissueList:
            if t+'.obj' not in filesList:
                missingLst.append(t)
        if missingLst != []:
            raise IndexError('Missing obj for {miss} in Geo Directory'.format(miss=missingLst))


        # When muscles are attached to each other. At least one muscle needs to remain in the scene.
        if len(cmds.ls(type = "zTissue")) > 1: # If more then one tissue in scene. Fat pass only has one tissue, and therfore can be updated independently. 
            if len(tissueList) == len(cmds.ls(type = "zTissue")):
                raise IndexError('Do not use this tool to update all muscles in the scene. Ziva requires at least one muscle to be present during the execution of this function. Instead save out a *.ziva file and rebuild')

        print('Updating: ', tissueList)

        # converts list to lowercase for comparison. & remove suffix (.obj)
        filesListLowercase = [file.lower()[:-4] for file in cmds.getFileList(folder=muscleDirPath, filespec='*.obj')]
        
        # Store tissue : tissue obj to dict if they match
        filesListDict = {}
        [filesListDict.update({tissue : filesListLowercase.index(tissue.lower())}) for tissue in tissueList if tissue.lower() in filesListLowercase]

        # Store muscle : muscle parent
        if filesListDict:
            tissueParentDict={}
            [tissueParentDict.update({key : cmds.listRelatives(key, p=True)[0]}) for key, value in filesListDict.items() if cmds.listRelatives(key, p=True)]

        # Store custom loa setup   
        customLOANodes = []
        for key, value in filesListDict.items():
            fiberList = mm.eval('zQuery -t "zFiber" {0}'.format(key)) # Check for existing fiber:
            if fiberList:
                for fiber in fiberList:
                    if cmds.listConnections(fiber+'.muscleGrowthScale'): # This port will be connected if using custom loa setup
                        customNodes = cmds.listHistory(cmds.listConnections(fiber+'.muscleGrowthScale'), f=0, levels=3)
                        for node in customNodes:
                            if '_LOACustomCurveInfo' in node:
                                customLOANodes.append(node)

        customLOADict = OrderedDict()
        if customLOANodes:
            for node in customLOANodes:
                if cmds.listConnections(node+'.inputCurve'): # Make sure curveInfo node is being used
                    customLOASettings = OrderedDict()
                    customLOASettings[node+'.growthScale']     = [cmds.getAttr(node+'.growthScale'), cmds.getAttr(node+'.growthScale', type=True)]
                    customLOASettings[node+'.loaFlexionChk']   = [cmds.getAttr(node+'.loaFlexionChk'), cmds.getAttr(node+'.loaFlexionChk', type=True)]
                    customLOASettings[node+'.loaExtensionChk'] = [cmds.getAttr(node+'.loaExtensionChk'), cmds.getAttr(node+'.loaExtensionChk', type=True)]
                    customLOASettings[node+'.flexionValue']    = [cmds.getAttr(node+'.flexionValue'), cmds.getAttr(node+'.flexionValue', type=True)]
                    customLOASettings[node+'.extensionValue']  = [cmds.getAttr(node+'.extensionValue'), cmds.getAttr(node+'.extensionValue', type=True)]

                    customLOADict['loaNodeBkup', cmds.getAttr(node+'.inputCrv'), cmds.getAttr(node+'.musFiber')] = customLOASettings

        # Get LOA curves if child of tissue
        loaCurveList = []
        materialDict = {}
        for key, value in filesListDict.items():
            # Check for nurbs curve with '_LOA_Curve' in the name
            [loaCurveList.append(loaCurve) for loaCurve in cmds.listRelatives(key, c=True) if ('_LOA_Curve' in loaCurve) and (cmds.objectType(omu.getDagPath(loaCurve, shape=True))=='nurbsCurve')]
            # Store materials
            mat = getMaterial(key)
            materialDict[key] = mat


        # Select filtered list of tissues and store ziva setup
        cmds.select(None)
        for key, value in filesListDict.items():
            cmds.select(key, add=True)
        z = zva.Ziva()
        z.retrieve_from_scene_selection()
        mm.eval("ziva -rm")

        # Un-parent any LOA curves if they exist
        if loaCurveList:
            zTool.loaUnParent(writeOut=False, loaCurves=loaCurveList)
        
        # Delete tissues
        for key, value in filesListDict.items():
            cmds.delete(key)

        # Import tissue obj's and rename them.
        for muscle, index in filesListDict.items():
            importFile = cmds.file(str(muscleDirPath) + '\\' + filesList[index] , i=True, returnNewNodes=True)
            cmds.select(importFile[0])
            cmds.polyNormalPerVertex(ufn=True)#unlock normals
            cmds.polySetToFaceNormal()#set normal to face
            cmds.rename(importFile[0], muscle)

        # Parent new tissue obj's
        for muscle, parent in tissueParentDict.items():
            cmds.parent(muscle, parent)

        # Reparent LOA curves
        if loaCurveList:
            zTool.loaReParent(readIn=False)

        # Build
        cmds.select(None)
        itemList = []
        for key, value in filesListDict.items():
            cmds.select(key, add=True)
            itemList.append(key)
        z.build()
        # Key run up
        zTool.keyRunUp(itemList, offFrame, onFrame, dampPadding)


        # Connect custom LOA setup
        if customLOADict:
            buildLOA = []
            for key , value in customLOADict.items():
                # if key[0] == 'loaNodeBkup':
                inputCrv = key[1] # curve
                zFiber = key[2] # fiber
                muscle = key[2][:-7]
                growthScale = list(value.items())[0][1] # growthScale
                loaFlexionChk = list(value.items())[1][1] # loaFlexionChk
                loaExtensionChk = list(value.items())[2][1] # loaExtensionChk
                flexionValue = list(value.items())[3][1] # flexionValue
                extensionValue = list(value.items())[4][1] # extensionValue
                buildLOA.append([flexionValue[0], extensionValue[0], growthScale[0], loaFlexionChk[0], loaExtensionChk[0], inputCrv, zFiber, muscle])
            else:
                pass

            if buildLOA:
                for build in buildLOA:
                    zTool.loaMuscleCustom(*build) # '*' will unpack list

        # Re-assign materials
        for key, value in materialDict.items():
            cmds.select(key, r=True)
            cmds.hyperShade(assign=value[0])

    if boneList != []:
        # Verify each selected Bone has corresponding .obj file to update to
        missingLst = []
        for b in boneList:
            if b+'.obj' not in filesList:
                missingLst.append(b)
        if missingLst != []:
            raise IndexError('Missing obj for {miss} in Geo Directory'.format(miss=missingLst))

        # converts list to lowercase for comparison. & remove suffix (.obj)
        filesListLowercase = [file.lower()[:-4] for file in cmds.getFileList(folder=muscleDirPath, filespec='*.obj')]
        
        # Store bone : bone obj to dict if they match
        filesListDict = {}
        [filesListDict.update({bone : filesListLowercase.index(bone.lower())}) for bone in boneList if bone.lower() in filesListLowercase]
        
        # Store bone : bone parent
        if filesListDict:
            boneParentDict={}
            [boneParentDict.update({key : cmds.listRelatives(key, p=True)[0]}) for key, value in filesListDict.items() if cmds.listRelatives(key, p=True)]

        # Store materials
        materialDict = {}
        for key, value in filesListDict.items():
            mat = getMaterial(key)
            materialDict[key] = mat

        # Select filtered list of bones and store ziva setup
        cmds.select(None)
        for key, value in filesListDict.items():
            cmds.select(key, add=True)
        z = zva.Ziva()
        z.retrieve_from_scene_selection()
        mm.eval("ziva -rm")

        # Delete bones
        for key, value in filesListDict.items():
            cmds.delete(key)

        # Import bone obj's and rename them.
        for bone, index in filesListDict.items():
            importFile = cmds.file(str(muscleDirPath) + '\\' + filesList[index] , i = True, returnNewNodes = True)
            cmds.rename(importFile[0], bone)

        # Parent new bone obj's
        for bone, parent in boneParentDict.items():
            cmds.parent(bone, parent)

        # Build
        cmds.select(None)
        for key, value in filesListDict.items():
            cmds.select(key, add=True)
        z.build()

        # Re-assign materials
        for key, value in materialDict.items():
            cmds.select(key, r=True)
            cmds.hyperShade(assign=value[0])

    cmds.select(None)