# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:/Working/dev/git/zivaTools/zivaToolsPy3/scripts/zWindow.ui',
# licensing of 'D:/Working/dev/git/zivaTools/zivaToolsPy3/scripts/zWindow.ui' applies.
#
# Created: Mon Dec 12 10:02:45 2022
#      by: pyside2-uic  running on PySide2 5.15.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_zivaWindow(object):
    def setupUi(self, zivaWindow):
        zivaWindow.setObjectName("zivaWindow")
        zivaWindow.resize(348, 806)
        zivaWindow.setStyleSheet("")
        self.centralwidget = QtWidgets.QWidget(zivaWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_51 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_51.setObjectName("gridLayout_51")
        self.UsrTet_Spn = QtWidgets.QTabWidget(self.centralwidget)
        self.UsrTet_Spn.setObjectName("UsrTet_Spn")
        self.zSetupTab = QtWidgets.QWidget()
        self.zSetupTab.setObjectName("zSetupTab")
        self.gridLayout_59 = QtWidgets.QGridLayout(self.zSetupTab)
        self.gridLayout_59.setObjectName("gridLayout_59")
        self.gridLayout_58 = QtWidgets.QGridLayout()
        self.gridLayout_58.setSpacing(3)
        self.gridLayout_58.setObjectName("gridLayout_58")
        self.groupBox = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color : rgb(248, 206, 0);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color : rgb(255, 225, 0);\n"
"}")
        self.groupBox.setObjectName("groupBox")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout_7.setSpacing(3)
        self.gridLayout_7.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.gridLayout_6 = QtWidgets.QGridLayout()
        self.gridLayout_6.setSpacing(3)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.addBone_Btn = QtWidgets.QPushButton(self.groupBox)
        self.addBone_Btn.setMinimumSize(QtCore.QSize(75, 20))
        self.addBone_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(246, 201, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(225, 222, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addBone_Btn.setObjectName("addBone_Btn")
        self.gridLayout_6.addWidget(self.addBone_Btn, 1, 0, 1, 1)
        self.addTissue_Btn = QtWidgets.QPushButton(self.groupBox)
        self.addTissue_Btn.setMinimumSize(QtCore.QSize(75, 20))
        self.addTissue_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(246, 201, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(225, 222, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addTissue_Btn.setObjectName("addTissue_Btn")
        self.gridLayout_6.addWidget(self.addTissue_Btn, 1, 1, 1, 1)
        self.addFiber_Btn = QtWidgets.QPushButton(self.groupBox)
        self.addFiber_Btn.setMinimumSize(QtCore.QSize(75, 20))
        self.addFiber_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(246, 201, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(225, 222, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addFiber_Btn.setObjectName("addFiber_Btn")
        self.gridLayout_6.addWidget(self.addFiber_Btn, 1, 2, 1, 1)
        self.addAttachment_Btn = QtWidgets.QPushButton(self.groupBox)
        self.addAttachment_Btn.setMinimumSize(QtCore.QSize(75, 20))
        self.addAttachment_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(244, 196, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(255, 217, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addAttachment_Btn.setObjectName("addAttachment_Btn")
        self.gridLayout_6.addWidget(self.addAttachment_Btn, 2, 0, 1, 1)
        self.addCloth_Btn = QtWidgets.QPushButton(self.groupBox)
        self.addCloth_Btn.setMinimumSize(QtCore.QSize(75, 20))
        self.addCloth_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(244, 196, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(255, 217, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addCloth_Btn.setObjectName("addCloth_Btn")
        self.gridLayout_6.addWidget(self.addCloth_Btn, 2, 1, 1, 1)
        self.addzCache_Btn = QtWidgets.QPushButton(self.groupBox)
        self.addzCache_Btn.setMinimumSize(QtCore.QSize(75, 20))
        self.addzCache_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(244, 196, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(255, 217, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addzCache_Btn.setObjectName("addzCache_Btn")
        self.gridLayout_6.addWidget(self.addzCache_Btn, 2, 2, 1, 1)
        self.addSolver_Btn = QtWidgets.QPushButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.addSolver_Btn.sizePolicy().hasHeightForWidth())
        self.addSolver_Btn.setSizePolicy(sizePolicy)
        self.addSolver_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.addSolver_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(249, 209, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(255, 229, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addSolver_Btn.setIconSize(QtCore.QSize(16, 16))
        self.addSolver_Btn.setObjectName("addSolver_Btn")
        self.gridLayout_6.addWidget(self.addSolver_Btn, 0, 0, 1, 3)
        self.gridLayout_7.addLayout(self.gridLayout_6, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox, 0, 0, 1, 1)
        self.groupBox_14 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_14.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"border-color:  rgb(239, 182, 0);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"}")
        self.groupBox_14.setTitle("")
        self.groupBox_14.setObjectName("groupBox_14")
        self.gridLayout_28 = QtWidgets.QGridLayout(self.groupBox_14)
        self.gridLayout_28.setSpacing(3)
        self.gridLayout_28.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_28.setObjectName("gridLayout_28")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setSpacing(3)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.selectProximity_Btn = QtWidgets.QPushButton(self.groupBox_14)
        self.selectProximity_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.selectProximity_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(239, 182, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(255, 200, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.selectProximity_Btn.setObjectName("selectProximity_Btn")
        self.gridLayout_3.addWidget(self.selectProximity_Btn, 0, 0, 1, 1)
        self.selectProximity_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_14)
        self.selectProximity_Spn.setMaximumSize(QtCore.QSize(60, 16777215))
        self.selectProximity_Spn.setSingleStep(0.1)
        self.selectProximity_Spn.setProperty("value", 0.1)
        self.selectProximity_Spn.setObjectName("selectProximity_Spn")
        self.gridLayout_3.addWidget(self.selectProximity_Spn, 0, 1, 1, 1)
        self.gridLayout_28.addLayout(self.gridLayout_3, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_14, 1, 0, 1, 1)
        self.groupBox_3 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_3.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color:  rgb(202, 80, 0);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color:  rgb(202, 80, 0);\n"
"}")
        self.groupBox_3.setObjectName("groupBox_3")
        self.gridLayout_19 = QtWidgets.QGridLayout(self.groupBox_3)
        self.gridLayout_19.setSpacing(3)
        self.gridLayout_19.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_19.setObjectName("gridLayout_19")
        self.gridLayout_18 = QtWidgets.QGridLayout()
        self.gridLayout_18.setSpacing(3)
        self.gridLayout_18.setObjectName("gridLayout_18")
        self.label = QtWidgets.QLabel(self.groupBox_3)
        self.label.setMinimumSize(QtCore.QSize(87, 0))
        self.label.setObjectName("label")
        self.gridLayout_18.addWidget(self.label, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_18.addItem(spacerItem, 0, 1, 1, 1)
        self.zTet_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_3)
        self.zTet_Spn.setMinimumSize(QtCore.QSize(60, 0))
        self.zTet_Spn.setMaximumSize(QtCore.QSize(60, 16777215))
        self.zTet_Spn.setSingleStep(0.1)
        self.zTet_Spn.setProperty("value", 1.0)
        self.zTet_Spn.setObjectName("zTet_Spn")
        self.gridLayout_18.addWidget(self.zTet_Spn, 0, 2, 1, 1)
        self.tetSize_Chk = QtWidgets.QCheckBox(self.groupBox_3)
        self.tetSize_Chk.setChecked(True)
        self.tetSize_Chk.setObjectName("tetSize_Chk")
        self.gridLayout_18.addWidget(self.tetSize_Chk, 0, 3, 1, 1)
        self.usrTetSize_Btn = QtWidgets.QPushButton(self.groupBox_3)
        self.usrTetSize_Btn.setMinimumSize(QtCore.QSize(35, 20))
        self.usrTetSize_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(233, 167, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(233, 197, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.usrTetSize_Btn.setObjectName("usrTetSize_Btn")
        self.gridLayout_18.addWidget(self.usrTetSize_Btn, 0, 4, 1, 1)
        self.gridLayout_19.addLayout(self.gridLayout_18, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_3, 2, 0, 1, 1)
        self.groupBox_4 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_4.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color: rgb(194, 62, 0);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color:  rgb(194, 62, 0);\n"
"}")
        self.groupBox_4.setObjectName("groupBox_4")
        self.gridLayout_17 = QtWidgets.QGridLayout(self.groupBox_4)
        self.gridLayout_17.setSpacing(3)
        self.gridLayout_17.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_17.setObjectName("gridLayout_17")
        self.gridLayout_13 = QtWidgets.QGridLayout()
        self.gridLayout_13.setSpacing(3)
        self.gridLayout_13.setObjectName("gridLayout_13")
        self.label_2 = QtWidgets.QLabel(self.groupBox_4)
        self.label_2.setMinimumSize(QtCore.QSize(87, 0))
        self.label_2.setObjectName("label_2")
        self.gridLayout_13.addWidget(self.label_2, 0, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(35, 17, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_13.addItem(spacerItem1, 0, 1, 1, 1)
        self.contactStiffness_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.contactStiffness_Spn.setMinimumSize(QtCore.QSize(60, 0))
        self.contactStiffness_Spn.setMaximumSize(QtCore.QSize(60, 16777215))
        self.contactStiffness_Spn.setSingleStep(0.1)
        self.contactStiffness_Spn.setProperty("value", 0.5)
        self.contactStiffness_Spn.setObjectName("contactStiffness_Spn")
        self.gridLayout_13.addWidget(self.contactStiffness_Spn, 0, 2, 1, 1)
        self.contactStiffness_Chk = QtWidgets.QCheckBox(self.groupBox_4)
        self.contactStiffness_Chk.setChecked(True)
        self.contactStiffness_Chk.setObjectName("contactStiffness_Chk")
        self.gridLayout_13.addWidget(self.contactStiffness_Chk, 0, 3, 1, 1)
        self.usrContactStiff_Btn = QtWidgets.QPushButton(self.groupBox_4)
        self.usrContactStiff_Btn.setMinimumSize(QtCore.QSize(35, 20))
        self.usrContactStiff_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(228, 154, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(228, 174, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.usrContactStiff_Btn.setObjectName("usrContactStiff_Btn")
        self.gridLayout_13.addWidget(self.usrContactStiff_Btn, 0, 4, 1, 1)
        self.gridLayout_17.addLayout(self.gridLayout_13, 0, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setSpacing(3)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.label_3 = QtWidgets.QLabel(self.groupBox_4)
        self.label_3.setMinimumSize(QtCore.QSize(87, 0))
        self.label_3.setObjectName("label_3")
        self.gridLayout_4.addWidget(self.label_3, 0, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(35, 17, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem2, 0, 1, 1, 1)
        self.compressionResistance_Spn = QtWidgets.QSpinBox(self.groupBox_4)
        self.compressionResistance_Spn.setMinimumSize(QtCore.QSize(60, 0))
        self.compressionResistance_Spn.setMaximumSize(QtCore.QSize(60, 16777215))
        self.compressionResistance_Spn.setMaximum(999999999)
        self.compressionResistance_Spn.setProperty("value", 1000)
        self.compressionResistance_Spn.setObjectName("compressionResistance_Spn")
        self.gridLayout_4.addWidget(self.compressionResistance_Spn, 0, 2, 1, 1)
        self.compressionResistance_Chk = QtWidgets.QCheckBox(self.groupBox_4)
        self.compressionResistance_Chk.setChecked(True)
        self.compressionResistance_Chk.setObjectName("compressionResistance_Chk")
        self.gridLayout_4.addWidget(self.compressionResistance_Chk, 0, 3, 1, 1)
        self.usrCompRes_Btn = QtWidgets.QPushButton(self.groupBox_4)
        self.usrCompRes_Btn.setMinimumSize(QtCore.QSize(35, 20))
        self.usrCompRes_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(226, 145, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(226, 165, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.usrCompRes_Btn.setObjectName("usrCompRes_Btn")
        self.gridLayout_4.addWidget(self.usrCompRes_Btn, 0, 4, 1, 1)
        self.gridLayout_17.addLayout(self.gridLayout_4, 1, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_4, 3, 0, 1, 1)
        self.groupBox_5 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_5.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color: rgb(185, 36, 0);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color:  rgb(185, 36, 0);\n"
"}")
        self.groupBox_5.setObjectName("groupBox_5")
        self.gridLayout_20 = QtWidgets.QGridLayout(self.groupBox_5)
        self.gridLayout_20.setSpacing(3)
        self.gridLayout_20.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_20.setObjectName("gridLayout_20")
        self.gridLayout_15 = QtWidgets.QGridLayout()
        self.gridLayout_15.setSpacing(3)
        self.gridLayout_15.setObjectName("gridLayout_15")
        self.label_4 = QtWidgets.QLabel(self.groupBox_5)
        self.label_4.setMinimumSize(QtCore.QSize(87, 0))
        self.label_4.setObjectName("label_4")
        self.gridLayout_15.addWidget(self.label_4, 0, 0, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(13, 17, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_15.addItem(spacerItem3, 0, 1, 1, 1)
        self.poissonsRatio_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_5)
        self.poissonsRatio_Spn.setMinimumSize(QtCore.QSize(60, 0))
        self.poissonsRatio_Spn.setMaximumSize(QtCore.QSize(60, 16777215))
        self.poissonsRatio_Spn.setMinimum(0.0)
        self.poissonsRatio_Spn.setMaximum(0.49)
        self.poissonsRatio_Spn.setSingleStep(0.1)
        self.poissonsRatio_Spn.setProperty("value", 0.3)
        self.poissonsRatio_Spn.setObjectName("poissonsRatio_Spn")
        self.gridLayout_15.addWidget(self.poissonsRatio_Spn, 0, 2, 1, 1)
        self.poissonsRatio_Chk = QtWidgets.QCheckBox(self.groupBox_5)
        self.poissonsRatio_Chk.setChecked(True)
        self.poissonsRatio_Chk.setObjectName("poissonsRatio_Chk")
        self.gridLayout_15.addWidget(self.poissonsRatio_Chk, 0, 3, 1, 1)
        self.usrPoisRatio_Btn = QtWidgets.QPushButton(self.groupBox_5)
        self.usrPoisRatio_Btn.setMinimumSize(QtCore.QSize(35, 20))
        self.usrPoisRatio_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(220, 130, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(220, 150, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.usrPoisRatio_Btn.setObjectName("usrPoisRatio_Btn")
        self.gridLayout_15.addWidget(self.usrPoisRatio_Btn, 0, 4, 1, 1)
        self.gridLayout_20.addLayout(self.gridLayout_15, 0, 0, 1, 1)
        self.gridLayout_27 = QtWidgets.QGridLayout()
        self.gridLayout_27.setSpacing(3)
        self.gridLayout_27.setObjectName("gridLayout_27")
        self.label_5 = QtWidgets.QLabel(self.groupBox_5)
        self.label_5.setMinimumSize(QtCore.QSize(87, 0))
        self.label_5.setObjectName("label_5")
        self.gridLayout_27.addWidget(self.label_5, 0, 0, 1, 1)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_27.addItem(spacerItem4, 0, 1, 1, 1)
        self.volumeCons_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_5)
        self.volumeCons_Spn.setMinimumSize(QtCore.QSize(60, 0))
        self.volumeCons_Spn.setMaximumSize(QtCore.QSize(60, 16777215))
        self.volumeCons_Spn.setMinimum(0.0)
        self.volumeCons_Spn.setMaximum(5.0)
        self.volumeCons_Spn.setSingleStep(0.1)
        self.volumeCons_Spn.setProperty("value", 1.0)
        self.volumeCons_Spn.setObjectName("volumeCons_Spn")
        self.gridLayout_27.addWidget(self.volumeCons_Spn, 0, 2, 1, 1)
        self.volumeCons_Chk = QtWidgets.QCheckBox(self.groupBox_5)
        self.volumeCons_Chk.setEnabled(True)
        self.volumeCons_Chk.setChecked(False)
        self.volumeCons_Chk.setObjectName("volumeCons_Chk")
        self.gridLayout_27.addWidget(self.volumeCons_Chk, 0, 3, 1, 1)
        self.usrVolumeCons_Btn = QtWidgets.QPushButton(self.groupBox_5)
        self.usrVolumeCons_Btn.setMinimumSize(QtCore.QSize(35, 20))
        self.usrVolumeCons_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(217, 122, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(217, 142, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.usrVolumeCons_Btn.setObjectName("usrVolumeCons_Btn")
        self.gridLayout_27.addWidget(self.usrVolumeCons_Btn, 0, 4, 1, 1)
        self.gridLayout_20.addLayout(self.gridLayout_27, 1, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_5, 4, 0, 1, 1)
        self.groupBox_18 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_18.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color:  rgb(214, 114, 0);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color:  rgb(214, 114, 0);\n"
"}")
        self.groupBox_18.setObjectName("groupBox_18")
        self.gridLayout_44 = QtWidgets.QGridLayout(self.groupBox_18)
        self.gridLayout_44.setSpacing(3)
        self.gridLayout_44.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_44.setObjectName("gridLayout_44")
        self.gridLayout_43 = QtWidgets.QGridLayout()
        self.gridLayout_43.setSpacing(3)
        self.gridLayout_43.setObjectName("gridLayout_43")
        self.selectAll_Btn = QtWidgets.QPushButton(self.groupBox_18)
        self.selectAll_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.selectAll_Btn.setMaximumSize(QtCore.QSize(75, 16777215))
        self.selectAll_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(211, 106, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(221, 126, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.selectAll_Btn.setObjectName("selectAll_Btn")
        self.gridLayout_43.addWidget(self.selectAll_Btn, 0, 2, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.groupBox_18)
        self.label_13.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_13.setObjectName("label_13")
        self.gridLayout_43.addWidget(self.label_13, 0, 0, 1, 1)
        self.selectAll_Combo = QtWidgets.QComboBox(self.groupBox_18)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.selectAll_Combo.sizePolicy().hasHeightForWidth())
        self.selectAll_Combo.setSizePolicy(sizePolicy)
        self.selectAll_Combo.setObjectName("selectAll_Combo")
        self.selectAll_Combo.addItem("")
        self.selectAll_Combo.addItem("")
        self.selectAll_Combo.addItem("")
        self.selectAll_Combo.addItem("")
        self.selectAll_Combo.addItem("")
        self.gridLayout_43.addWidget(self.selectAll_Combo, 0, 1, 1, 1)
        self.gridLayout_44.addLayout(self.gridLayout_43, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_18, 5, 0, 1, 1)
        self.groupBox_11 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_11.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color:  rgb(232, 163, 0);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color: rgb(232, 163, 0);\n"
"}")
        self.groupBox_11.setObjectName("groupBox_11")
        self.gridLayout_14 = QtWidgets.QGridLayout(self.groupBox_11)
        self.gridLayout_14.setSpacing(3)
        self.gridLayout_14.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_14.setObjectName("gridLayout_14")
        self.gridLayout_12 = QtWidgets.QGridLayout()
        self.gridLayout_12.setSpacing(3)
        self.gridLayout_12.setObjectName("gridLayout_12")
        self.computeVol_Btn = QtWidgets.QPushButton(self.groupBox_11)
        self.computeVol_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.computeVol_Btn.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.computeVol_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(202, 81, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(202, 101, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.computeVol_Btn.setObjectName("computeVol_Btn")
        self.gridLayout_12.addWidget(self.computeVol_Btn, 1, 0, 1, 1)
        self.toggleMuscle_Btn = QtWidgets.QPushButton(self.groupBox_11)
        self.toggleMuscle_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.toggleMuscle_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(199, 74, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(199, 94, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.toggleMuscle_Btn.setObjectName("toggleMuscle_Btn")
        self.gridLayout_12.addWidget(self.toggleMuscle_Btn, 2, 0, 1, 1)
        self.renameNodes_Btn = QtWidgets.QPushButton(self.groupBox_11)
        self.renameNodes_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.renameNodes_Btn.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.renameNodes_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(202, 81, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(202, 101, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.renameNodes_Btn.setObjectName("renameNodes_Btn")
        self.gridLayout_12.addWidget(self.renameNodes_Btn, 1, 1, 1, 1)
        self.zBuilderWindow_Btn = QtWidgets.QPushButton(self.groupBox_11)
        self.zBuilderWindow_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.zBuilderWindow_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(199, 74, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(199, 94, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.zBuilderWindow_Btn.setObjectName("zBuilderWindow_Btn")
        self.gridLayout_12.addWidget(self.zBuilderWindow_Btn, 2, 1, 1, 1)
        self.mirrorMuscleLR_Btn = QtWidgets.QPushButton(self.groupBox_11)
        self.mirrorMuscleLR_Btn.setMinimumSize(QtCore.QSize(200, 20))
        self.mirrorMuscleLR_Btn.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.mirrorMuscleLR_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(196, 66, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(196, 86, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.mirrorMuscleLR_Btn.setObjectName("mirrorMuscleLR_Btn")
        self.gridLayout_12.addWidget(self.mirrorMuscleLR_Btn, 3, 0, 1, 2)
        self.meshCheck_Btn = QtWidgets.QPushButton(self.groupBox_11)
        self.meshCheck_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.meshCheck_Btn.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.meshCheck_Btn.setToolTip("")
        self.meshCheck_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(205, 89, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(205, 109, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.meshCheck_Btn.setObjectName("meshCheck_Btn")
        self.gridLayout_12.addWidget(self.meshCheck_Btn, 0, 0, 1, 1)
        self.findIntersections_Btn = QtWidgets.QPushButton(self.groupBox_11)
        self.findIntersections_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.findIntersections_Btn.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.findIntersections_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(205, 89, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(205, 109, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.findIntersections_Btn.setObjectName("findIntersections_Btn")
        self.gridLayout_12.addWidget(self.findIntersections_Btn, 0, 1, 1, 1)
        self.gridLayout_14.addLayout(self.gridLayout_12, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_11, 6, 0, 1, 1)
        self.groupBox_23 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_23.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color: rgb(177, 14, 0);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color:  rgb(177, 14, 0);\n"
"}")
        self.groupBox_23.setObjectName("groupBox_23")
        self.gridLayout_49 = QtWidgets.QGridLayout(self.groupBox_23)
        self.gridLayout_49.setSpacing(3)
        self.gridLayout_49.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_49.setObjectName("gridLayout_49")
        self.gridLayout_48 = QtWidgets.QGridLayout()
        self.gridLayout_48.setSpacing(3)
        self.gridLayout_48.setObjectName("gridLayout_48")
        self.StartFrame = QtWidgets.QLabel(self.groupBox_23)
        self.StartFrame.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.StartFrame.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.StartFrame.setObjectName("StartFrame")
        self.gridLayout_48.addWidget(self.StartFrame, 0, 0, 1, 1)
        self.saveCacheStart_Spn = QtWidgets.QSpinBox(self.groupBox_23)
        self.saveCacheStart_Spn.setMinimum(-500)
        self.saveCacheStart_Spn.setMaximum(999999)
        self.saveCacheStart_Spn.setProperty("value", 0)
        self.saveCacheStart_Spn.setObjectName("saveCacheStart_Spn")
        self.gridLayout_48.addWidget(self.saveCacheStart_Spn, 0, 1, 1, 1)
        self.EndFrame = QtWidgets.QLabel(self.groupBox_23)
        self.EndFrame.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.EndFrame.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.EndFrame.setObjectName("EndFrame")
        self.gridLayout_48.addWidget(self.EndFrame, 0, 2, 1, 1)
        self.saveCacheEnd_Spn = QtWidgets.QSpinBox(self.groupBox_23)
        self.saveCacheEnd_Spn.setMinimum(-500)
        self.saveCacheEnd_Spn.setMaximum(999999)
        self.saveCacheEnd_Spn.setProperty("value", 10)
        self.saveCacheEnd_Spn.setObjectName("saveCacheEnd_Spn")
        self.gridLayout_48.addWidget(self.saveCacheEnd_Spn, 0, 3, 1, 1)
        self.saveCache_Btn = QtWidgets.QPushButton(self.groupBox_23)
        self.saveCache_Btn.setMinimumSize(QtCore.QSize(200, 20))
        self.saveCache_Btn.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.saveCache_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(188, 43, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(190, 83, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.saveCache_Btn.setObjectName("saveCache_Btn")
        self.gridLayout_48.addWidget(self.saveCache_Btn, 1, 0, 1, 4)
        self.gridLayout_49.addLayout(self.gridLayout_48, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_23, 7, 0, 1, 1)
        self.groupBox_24 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_24.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color: rgb(177, 14, 0);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color:  rgb(177, 14, 0);\n"
"}")
        self.groupBox_24.setObjectName("groupBox_24")
        self.gridLayout_57 = QtWidgets.QGridLayout(self.groupBox_24)
        self.gridLayout_57.setSpacing(3)
        self.gridLayout_57.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_57.setObjectName("gridLayout_57")
        self.gridLayout_56 = QtWidgets.QGridLayout()
        self.gridLayout_56.setSpacing(3)
        self.gridLayout_56.setObjectName("gridLayout_56")
        self.StartFrame_2 = QtWidgets.QLabel(self.groupBox_24)
        self.StartFrame_2.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.StartFrame_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.StartFrame_2.setObjectName("StartFrame_2")
        self.gridLayout_56.addWidget(self.StartFrame_2, 0, 0, 1, 1)
        self.loadCacheStart_Spn = QtWidgets.QSpinBox(self.groupBox_24)
        self.loadCacheStart_Spn.setMinimum(-500)
        self.loadCacheStart_Spn.setMaximum(99999)
        self.loadCacheStart_Spn.setProperty("value", 0)
        self.loadCacheStart_Spn.setObjectName("loadCacheStart_Spn")
        self.gridLayout_56.addWidget(self.loadCacheStart_Spn, 0, 1, 1, 1)
        self.EndFrame_2 = QtWidgets.QLabel(self.groupBox_24)
        self.EndFrame_2.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.EndFrame_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.EndFrame_2.setObjectName("EndFrame_2")
        self.gridLayout_56.addWidget(self.EndFrame_2, 0, 2, 1, 1)
        self.loadCacheEnd_Spn = QtWidgets.QSpinBox(self.groupBox_24)
        self.loadCacheEnd_Spn.setMinimum(-500)
        self.loadCacheEnd_Spn.setMaximum(99999)
        self.loadCacheEnd_Spn.setProperty("value", 10)
        self.loadCacheEnd_Spn.setObjectName("loadCacheEnd_Spn")
        self.gridLayout_56.addWidget(self.loadCacheEnd_Spn, 0, 3, 1, 1)
        self.loadCache_Btn = QtWidgets.QPushButton(self.groupBox_24)
        self.loadCache_Btn.setMinimumSize(QtCore.QSize(200, 20))
        self.loadCache_Btn.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.loadCache_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(180, 24, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(195, 54, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.loadCache_Btn.setObjectName("loadCache_Btn")
        self.gridLayout_56.addWidget(self.loadCache_Btn, 1, 0, 1, 4)
        self.gridLayout_57.addLayout(self.gridLayout_56, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_24, 8, 0, 1, 1)
        self.groupBox_2 = QtWidgets.QGroupBox(self.zSetupTab)
        self.groupBox_2.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color: rgb(177, 14, 0);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color:  rgb(177, 14, 0);\n"
"}")
        self.groupBox_2.setObjectName("groupBox_2")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.groupBox_2)
        self.gridLayout_5.setSpacing(3)
        self.gridLayout_5.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSpacing(3)
        self.gridLayout.setObjectName("gridLayout")
        self.delAttachment_Btn = QtWidgets.QPushButton(self.groupBox_2)
        self.delAttachment_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.delAttachment_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(177, 14, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(195, 15, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.delAttachment_Btn.setObjectName("delAttachment_Btn")
        self.gridLayout.addWidget(self.delAttachment_Btn, 0, 0, 1, 1)
        self.delFiber_Btn = QtWidgets.QPushButton(self.groupBox_2)
        self.delFiber_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.delFiber_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(177, 14, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(195, 15, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.delFiber_Btn.setObjectName("delFiber_Btn")
        self.gridLayout.addWidget(self.delFiber_Btn, 0, 1, 1, 1)
        self.delzCache_Btn = QtWidgets.QPushButton(self.groupBox_2)
        self.delzCache_Btn.setEnabled(True)
        self.delzCache_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.delzCache_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(177, 14, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(195, 15, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.delzCache_Btn.setObjectName("delzCache_Btn")
        self.gridLayout.addWidget(self.delzCache_Btn, 0, 2, 1, 1)
        self.clearSelected_Btn = QtWidgets.QPushButton(self.groupBox_2)
        self.clearSelected_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.clearSelected_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(176, 10, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(193, 12, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.clearSelected_Btn.setObjectName("clearSelected_Btn")
        self.gridLayout.addWidget(self.clearSelected_Btn, 1, 0, 1, 1)
        self.clearScene_Btn = QtWidgets.QPushButton(self.groupBox_2)
        self.clearScene_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.clearScene_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(176, 10, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(193, 12, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.clearScene_Btn.setObjectName("clearScene_Btn")
        self.gridLayout.addWidget(self.clearScene_Btn, 1, 1, 1, 1)
        self.clearCache_Btn = QtWidgets.QPushButton(self.groupBox_2)
        self.clearCache_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.clearCache_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(176, 10, 0);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(193, 12, 0);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.clearCache_Btn.setObjectName("clearCache_Btn")
        self.gridLayout.addWidget(self.clearCache_Btn, 1, 2, 1, 1)
        self.gridLayout_5.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.gridLayout_58.addWidget(self.groupBox_2, 9, 0, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(296, 28, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_58.addItem(spacerItem5, 10, 0, 1, 1)
        self.gridLayout_59.addLayout(self.gridLayout_58, 0, 0, 1, 1)
        self.UsrTet_Spn.addTab(self.zSetupTab, "")
        self.LOATab = QtWidgets.QWidget()
        self.LOATab.setObjectName("LOATab")
        self.gridLayout_55 = QtWidgets.QGridLayout(self.LOATab)
        self.gridLayout_55.setObjectName("gridLayout_55")
        self.gridLayout_54 = QtWidgets.QGridLayout()
        self.gridLayout_54.setSpacing(3)
        self.gridLayout_54.setObjectName("gridLayout_54")
        self.groupBox_16 = QtWidgets.QGroupBox(self.LOATab)
        self.groupBox_16.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color : rgb(116, 224, 8);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color :rgb(116, 224, 8);\n"
"}")
        self.groupBox_16.setObjectName("groupBox_16")
        self.gridLayout_32 = QtWidgets.QGridLayout(self.groupBox_16)
        self.gridLayout_32.setSpacing(3)
        self.gridLayout_32.setContentsMargins(6, 6, 6, 6)
        self.gridLayout_32.setObjectName("gridLayout_32")
        self.gridLayout_30 = QtWidgets.QGridLayout()
        self.gridLayout_30.setSpacing(3)
        self.gridLayout_30.setObjectName("gridLayout_30")
        self.createLOA_Btn = QtWidgets.QPushButton(self.groupBox_16)
        self.createLOA_Btn.setEnabled(True)
        self.createLOA_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.createLOA_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(118, 228, 7);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(135, 255, 11);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.createLOA_Btn.setObjectName("createLOA_Btn")
        self.gridLayout_30.addWidget(self.createLOA_Btn, 0, 0, 1, 1)
        self.clusterOnCurve_Btn = QtWidgets.QPushButton(self.groupBox_16)
        self.clusterOnCurve_Btn.setEnabled(True)
        self.clusterOnCurve_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.clusterOnCurve_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(115, 224, 9);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(131, 255, 13);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.clusterOnCurve_Btn.setObjectName("clusterOnCurve_Btn")
        self.gridLayout_30.addWidget(self.clusterOnCurve_Btn, 1, 0, 1, 1)
        self.clusterOnCV_Btn = QtWidgets.QPushButton(self.groupBox_16)
        self.clusterOnCV_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.clusterOnCV_Btn.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.clusterOnCV_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(112, 220, 9);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(128, 253, 14);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.clusterOnCV_Btn.setObjectName("clusterOnCV_Btn")
        self.gridLayout_30.addWidget(self.clusterOnCV_Btn, 2, 0, 1, 1)
        self.LOARivet_Btn = QtWidgets.QPushButton(self.groupBox_16)
        self.LOARivet_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOARivet_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(109, 217, 10);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(123, 248, 15);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOARivet_Btn.setObjectName("LOARivet_Btn")
        self.gridLayout_30.addWidget(self.LOARivet_Btn, 3, 0, 1, 1)
        self.LOAAutoRivet_Btn = QtWidgets.QPushButton(self.groupBox_16)
        self.LOAAutoRivet_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOAAutoRivet_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(106, 213, 11);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(123, 248, 15);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOAAutoRivet_Btn.setObjectName("LOAAutoRivet_Btn")
        self.gridLayout_30.addWidget(self.LOAAutoRivet_Btn, 4, 0, 1, 1)
        self.gridLayout_32.addLayout(self.gridLayout_30, 0, 0, 1, 1)
        self.gridLayout_54.addWidget(self.groupBox_16, 0, 0, 1, 1)
        self.groupBox_12 = QtWidgets.QGroupBox(self.LOATab)
        self.groupBox_12.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color :  rgb(90, 195, 15);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color :   rgb(90, 195, 15);\n"
"}")
        self.groupBox_12.setObjectName("groupBox_12")
        self.gridLayout_29 = QtWidgets.QGridLayout(self.groupBox_12)
        self.gridLayout_29.setSpacing(3)
        self.gridLayout_29.setObjectName("gridLayout_29")
        self.gridLayout_16 = QtWidgets.QGridLayout()
        self.gridLayout_16.setSpacing(3)
        self.gridLayout_16.setObjectName("gridLayout_16")
        self.loaFlexion_Chk = QtWidgets.QCheckBox(self.groupBox_12)
        self.loaFlexion_Chk.setMaximumSize(QtCore.QSize(500, 16777215))
        self.loaFlexion_Chk.setChecked(True)
        self.loaFlexion_Chk.setObjectName("loaFlexion_Chk")
        self.gridLayout_16.addWidget(self.loaFlexion_Chk, 0, 0, 1, 1)
        self.curveFlexionLength_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_12)
        self.curveFlexionLength_Spn.setDecimals(2)
        self.curveFlexionLength_Spn.setMaximum(999.0)
        self.curveFlexionLength_Spn.setSingleStep(0.1)
        self.curveFlexionLength_Spn.setProperty("value", 0.0)
        self.curveFlexionLength_Spn.setObjectName("curveFlexionLength_Spn")
        self.gridLayout_16.addWidget(self.curveFlexionLength_Spn, 0, 1, 1, 1)
        self.LOACurrentLengthFlexion_Btn = QtWidgets.QPushButton(self.groupBox_12)
        self.LOACurrentLengthFlexion_Btn.setEnabled(True)
        self.LOACurrentLengthFlexion_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOACurrentLengthFlexion_Btn.setMaximumSize(QtCore.QSize(100, 16777215))
        self.LOACurrentLengthFlexion_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(97, 204, 13);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(112, 236, 16);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOACurrentLengthFlexion_Btn.setObjectName("LOACurrentLengthFlexion_Btn")
        self.gridLayout_16.addWidget(self.LOACurrentLengthFlexion_Btn, 0, 2, 1, 1)
        self.loaExtension_Chk = QtWidgets.QCheckBox(self.groupBox_12)
        self.loaExtension_Chk.setMaximumSize(QtCore.QSize(500, 16777210))
        self.loaExtension_Chk.setChecked(True)
        self.loaExtension_Chk.setObjectName("loaExtension_Chk")
        self.gridLayout_16.addWidget(self.loaExtension_Chk, 1, 0, 1, 1)
        self.curveExtensionLength_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_12)
        self.curveExtensionLength_Spn.setDecimals(2)
        self.curveExtensionLength_Spn.setMaximum(999.0)
        self.curveExtensionLength_Spn.setSingleStep(0.1)
        self.curveExtensionLength_Spn.setProperty("value", 0.0)
        self.curveExtensionLength_Spn.setObjectName("curveExtensionLength_Spn")
        self.gridLayout_16.addWidget(self.curveExtensionLength_Spn, 1, 1, 1, 1)
        self.LOACurrentLengthExtension_Btn = QtWidgets.QPushButton(self.groupBox_12)
        self.LOACurrentLengthExtension_Btn.setEnabled(True)
        self.LOACurrentLengthExtension_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOACurrentLengthExtension_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(93, 199, 14);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(107, 231, 16);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOACurrentLengthExtension_Btn.setObjectName("LOACurrentLengthExtension_Btn")
        self.gridLayout_16.addWidget(self.LOACurrentLengthExtension_Btn, 1, 2, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.groupBox_12)
        self.label_12.setObjectName("label_12")
        self.gridLayout_16.addWidget(self.label_12, 2, 0, 1, 1)
        self.muscleGrowth_Spn = QtWidgets.QDoubleSpinBox(self.groupBox_12)
        self.muscleGrowth_Spn.setDecimals(2)
        self.muscleGrowth_Spn.setMaximum(25.0)
        self.muscleGrowth_Spn.setSingleStep(0.1)
        self.muscleGrowth_Spn.setProperty("value", 1.0)
        self.muscleGrowth_Spn.setObjectName("muscleGrowth_Spn")
        self.gridLayout_16.addWidget(self.muscleGrowth_Spn, 2, 1, 1, 1)
        self.LOAMuscleCustom_Btn = QtWidgets.QPushButton(self.groupBox_12)
        self.LOAMuscleCustom_Btn.setEnabled(True)
        self.LOAMuscleCustom_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOAMuscleCustom_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(85, 190, 16);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(96, 219, 20);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOAMuscleCustom_Btn.setObjectName("LOAMuscleCustom_Btn")
        self.gridLayout_16.addWidget(self.LOAMuscleCustom_Btn, 3, 0, 1, 3)
        self.line_3 = QtWidgets.QFrame(self.groupBox_12)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout_16.addWidget(self.line_3, 5, 0, 1, 3)
        self.LOAMuscleCustomReconnect_Btn = QtWidgets.QPushButton(self.groupBox_12)
        self.LOAMuscleCustomReconnect_Btn.setEnabled(True)
        self.LOAMuscleCustomReconnect_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOAMuscleCustomReconnect_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(75, 178, 19);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(83, 205, 23);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOAMuscleCustomReconnect_Btn.setObjectName("LOAMuscleCustomReconnect_Btn")
        self.gridLayout_16.addWidget(self.LOAMuscleCustomReconnect_Btn, 6, 0, 1, 3)
        self.LOAMuscleCustomDel_Btn = QtWidgets.QPushButton(self.groupBox_12)
        self.LOAMuscleCustomDel_Btn.setEnabled(True)
        self.LOAMuscleCustomDel_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOAMuscleCustomDel_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(80, 185, 17);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(90, 212, 22);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOAMuscleCustomDel_Btn.setObjectName("LOAMuscleCustomDel_Btn")
        self.gridLayout_16.addWidget(self.LOAMuscleCustomDel_Btn, 4, 0, 1, 3)
        self.gridLayout_29.addLayout(self.gridLayout_16, 0, 0, 1, 1)
        self.gridLayout_54.addWidget(self.groupBox_12, 1, 0, 1, 1)
        self.groupBox_15 = QtWidgets.QGroupBox(self.LOATab)
        self.groupBox_15.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color : rgb(103, 211, 11);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color :  rgb(103, 211, 11);\n"
"}")
        self.groupBox_15.setObjectName("groupBox_15")
        self.gridLayout_39 = QtWidgets.QGridLayout(self.groupBox_15)
        self.gridLayout_39.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_39.setHorizontalSpacing(3)
        self.gridLayout_39.setObjectName("gridLayout_39")
        self.gridLayout_36 = QtWidgets.QGridLayout()
        self.gridLayout_36.setSpacing(3)
        self.gridLayout_36.setObjectName("gridLayout_36")
        self.LOAMuscle_Btn = QtWidgets.QPushButton(self.groupBox_15)
        self.LOAMuscle_Btn.setEnabled(True)
        self.LOAMuscle_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOAMuscle_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(64, 166, 21);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(71, 190, 27);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOAMuscle_Btn.setObjectName("LOAMuscle_Btn")
        self.gridLayout_36.addWidget(self.LOAMuscle_Btn, 0, 0, 1, 1)
        self.LOADelete_Btn = QtWidgets.QPushButton(self.groupBox_15)
        self.LOADelete_Btn.setEnabled(True)
        self.LOADelete_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.LOADelete_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(60, 162, 23);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(67, 185, 29);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.LOADelete_Btn.setObjectName("LOADelete_Btn")
        self.gridLayout_36.addWidget(self.LOADelete_Btn, 1, 0, 1, 1)
        self.gridLayout_39.addLayout(self.gridLayout_36, 0, 0, 1, 1)
        self.gridLayout_54.addWidget(self.groupBox_15, 2, 0, 1, 1)
        self.groupBox_17 = QtWidgets.QGroupBox(self.LOATab)
        self.groupBox_17.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color :  rgb(58, 159, 24);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color : rgb(58, 159, 24);\n"
"}")
        self.groupBox_17.setObjectName("groupBox_17")
        self.gridLayout_37 = QtWidgets.QGridLayout(self.groupBox_17)
        self.gridLayout_37.setSpacing(3)
        self.gridLayout_37.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_37.setObjectName("gridLayout_37")
        self.gridLayout_33 = QtWidgets.QGridLayout()
        self.gridLayout_33.setSpacing(3)
        self.gridLayout_33.setObjectName("gridLayout_33")
        self.LOAUnParent_Btn = QtWidgets.QPushButton(self.groupBox_17)
        self.LOAUnParent_Btn.setMinimumSize(QtCore.QSize(185, 20))
        self.LOAUnParent_Btn.setStatusTip("")
        self.LOAUnParent_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(50, 151, 26);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(54, 171, 32);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.LOAUnParent_Btn.setObjectName("LOAUnParent_Btn")
        self.gridLayout_33.addWidget(self.LOAUnParent_Btn, 0, 0, 1, 1)
        self.writeOut_Chk = QtWidgets.QCheckBox(self.groupBox_17)
        self.writeOut_Chk.setMaximumSize(QtCore.QSize(80, 16777215))
        self.writeOut_Chk.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.writeOut_Chk.setChecked(False)
        self.writeOut_Chk.setTristate(False)
        self.writeOut_Chk.setObjectName("writeOut_Chk")
        self.gridLayout_33.addWidget(self.writeOut_Chk, 0, 1, 1, 1)
        self.LOAReParent_Btn = QtWidgets.QPushButton(self.groupBox_17)
        self.LOAReParent_Btn.setMinimumSize(QtCore.QSize(185, 20))
        self.LOAReParent_Btn.setStatusTip("")
        self.LOAReParent_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(46, 146, 28);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(54, 171, 32);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.LOAReParent_Btn.setObjectName("LOAReParent_Btn")
        self.gridLayout_33.addWidget(self.LOAReParent_Btn, 1, 0, 1, 1)
        self.readIn_Chk = QtWidgets.QCheckBox(self.groupBox_17)
        self.readIn_Chk.setMaximumSize(QtCore.QSize(80, 16777215))
        self.readIn_Chk.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.readIn_Chk.setObjectName("readIn_Chk")
        self.gridLayout_33.addWidget(self.readIn_Chk, 1, 1, 1, 1)
        self.gridLayout_37.addLayout(self.gridLayout_33, 0, 0, 1, 1)
        self.gridLayout_54.addWidget(self.groupBox_17, 3, 0, 1, 1)
        self.groupBox_20 = QtWidgets.QGroupBox(self.LOATab)
        self.groupBox_20.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color :  rgb(58, 159, 24);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color : rgb(58, 159, 24);\n"
"}")
        self.groupBox_20.setObjectName("groupBox_20")
        self.gridLayout_41 = QtWidgets.QGridLayout(self.groupBox_20)
        self.gridLayout_41.setSpacing(3)
        self.gridLayout_41.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_41.setObjectName("gridLayout_41")
        self.gridLayout_40 = QtWidgets.QGridLayout()
        self.gridLayout_40.setSpacing(3)
        self.gridLayout_40.setObjectName("gridLayout_40")
        self.LOABackup_Btn = QtWidgets.QPushButton(self.groupBox_20)
        self.LOABackup_Btn.setMinimumSize(QtCore.QSize(185, 20))
        self.LOABackup_Btn.setStatusTip("")
        self.LOABackup_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(36, 135, 30);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(54, 171, 32);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.LOABackup_Btn.setObjectName("LOABackup_Btn")
        self.gridLayout_40.addWidget(self.LOABackup_Btn, 0, 0, 1, 1)
        self.LOARestore_Btn = QtWidgets.QPushButton(self.groupBox_20)
        self.LOARestore_Btn.setMinimumSize(QtCore.QSize(185, 20))
        self.LOARestore_Btn.setStatusTip("")
        self.LOARestore_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(33, 130, 31);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(54, 171, 32);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"")
        self.LOARestore_Btn.setObjectName("LOARestore_Btn")
        self.gridLayout_40.addWidget(self.LOARestore_Btn, 1, 0, 1, 1)
        self.gridLayout_41.addLayout(self.gridLayout_40, 0, 0, 1, 1)
        self.gridLayout_54.addWidget(self.groupBox_20, 4, 0, 1, 1)
        self.groupBox_21 = QtWidgets.QGroupBox(self.LOATab)
        self.groupBox_21.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color :  rgb(58, 159, 24);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color : rgb(58, 159, 24);\n"
"}")
        self.groupBox_21.setTitle("")
        self.groupBox_21.setObjectName("groupBox_21")
        self.gridLayout_53 = QtWidgets.QGridLayout(self.groupBox_21)
        self.gridLayout_53.setSpacing(3)
        self.gridLayout_53.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_53.setObjectName("gridLayout_53")
        self.toggleLOACurve_Btn = QtWidgets.QPushButton(self.groupBox_21)
        self.toggleLOACurve_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.toggleLOACurve_Btn.setMaximumSize(QtCore.QSize(5000, 16777215))
        self.toggleLOACurve_Btn.setStatusTip("")
        self.toggleLOACurve_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(42, 141, 29);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(49, 166, 33);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.toggleLOACurve_Btn.setObjectName("toggleLOACurve_Btn")
        self.gridLayout_53.addWidget(self.toggleLOACurve_Btn, 0, 0, 1, 1)
        self.gridLayout_54.addWidget(self.groupBox_21, 5, 0, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_54.addItem(spacerItem6, 6, 0, 1, 1)
        self.gridLayout_55.addLayout(self.gridLayout_54, 0, 0, 1, 1)
        self.UsrTet_Spn.addTab(self.LOATab, "")
        self.zToolsTab = QtWidgets.QWidget()
        self.zToolsTab.setObjectName("zToolsTab")
        self.gridLayout_52 = QtWidgets.QGridLayout(self.zToolsTab)
        self.gridLayout_52.setObjectName("gridLayout_52")
        self.gridLayout_46 = QtWidgets.QGridLayout()
        self.gridLayout_46.setSpacing(3)
        self.gridLayout_46.setObjectName("gridLayout_46")
        self.groupBox_6 = QtWidgets.QGroupBox(self.zToolsTab)
        self.groupBox_6.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color: rgb(8, 250, 255);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color: rgb(8, 250, 255);\n"
"}")
        self.groupBox_6.setObjectName("groupBox_6")
        self.gridLayout_22 = QtWidgets.QGridLayout(self.groupBox_6)
        self.gridLayout_22.setSpacing(3)
        self.gridLayout_22.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_22.setObjectName("gridLayout_22")
        self.gridLayout_21 = QtWidgets.QGridLayout()
        self.gridLayout_21.setSpacing(3)
        self.gridLayout_21.setObjectName("gridLayout_21")
        self.browseMuscleGeoDir_Btn = QtWidgets.QPushButton(self.groupBox_6)
        self.browseMuscleGeoDir_Btn.setMinimumSize(QtCore.QSize(50, 20))
        self.browseMuscleGeoDir_Btn.setMaximumSize(QtCore.QSize(50, 16777215))
        self.browseMuscleGeoDir_Btn.setStyleSheet("QPushButton{\n"
"    background-color: rgb(90, 90, 90);\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(110, 110, 110);\n"
"}")
        self.browseMuscleGeoDir_Btn.setObjectName("browseMuscleGeoDir_Btn")
        self.gridLayout_21.addWidget(self.browseMuscleGeoDir_Btn, 0, 0, 1, 1)
        self.updateGeoPath_Txt = QtWidgets.QLineEdit(self.groupBox_6)
        self.updateGeoPath_Txt.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.updateGeoPath_Txt.setStyleSheet("")
        self.updateGeoPath_Txt.setText("")
        self.updateGeoPath_Txt.setFrame(True)
        self.updateGeoPath_Txt.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.updateGeoPath_Txt.setPlaceholderText("")
        self.updateGeoPath_Txt.setObjectName("updateGeoPath_Txt")
        self.gridLayout_21.addWidget(self.updateGeoPath_Txt, 0, 1, 1, 1)
        self.updateMuscleGeo_Btn = QtWidgets.QPushButton(self.groupBox_6)
        self.updateMuscleGeo_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.updateMuscleGeo_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(10, 236, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(14, 255, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.updateMuscleGeo_Btn.setObjectName("updateMuscleGeo_Btn")
        self.gridLayout_21.addWidget(self.updateMuscleGeo_Btn, 1, 0, 1, 2)
        self.gridLayout_22.addLayout(self.gridLayout_21, 0, 0, 1, 1)
        self.gridLayout_46.addWidget(self.groupBox_6, 0, 0, 1, 1)
        self.groupBox_10 = QtWidgets.QGroupBox(self.zToolsTab)
        self.groupBox_10.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    border-color: rgb(16, 203, 255);\n"
"    margin-top: 0.5em;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color: rgb(16, 203, 255);\n"
"}")
        self.groupBox_10.setObjectName("groupBox_10")
        self.gridLayout_24 = QtWidgets.QGridLayout(self.groupBox_10)
        self.gridLayout_24.setSpacing(3)
        self.gridLayout_24.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_24.setObjectName("gridLayout_24")
        self.gridLayout_23 = QtWidgets.QGridLayout()
        self.gridLayout_23.setSpacing(3)
        self.gridLayout_23.setObjectName("gridLayout_23")
        self.storeSceneSetup_Btn = QtWidgets.QPushButton(self.groupBox_10)
        self.storeSceneSetup_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.storeSceneSetup_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(16, 203, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(18, 236\n"
", 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.storeSceneSetup_Btn.setObjectName("storeSceneSetup_Btn")
        self.gridLayout_23.addWidget(self.storeSceneSetup_Btn, 1, 0, 1, 2)
        self.retrieveSetup_Btn = QtWidgets.QPushButton(self.groupBox_10)
        self.retrieveSetup_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.retrieveSetup_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(18, 188, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(22, 219, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.retrieveSetup_Btn.setObjectName("retrieveSetup_Btn")
        self.gridLayout_23.addWidget(self.retrieveSetup_Btn, 3, 0, 1, 2)
        self.line_2 = QtWidgets.QFrame(self.groupBox_10)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout_23.addWidget(self.line_2, 2, 0, 1, 2)
        self.storeSelSetup_Btn = QtWidgets.QPushButton(self.groupBox_10)
        self.storeSelSetup_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.storeSelSetup_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(16, 203, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(18, 236\n"
", 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.storeSelSetup_Btn.setObjectName("storeSelSetup_Btn")
        self.gridLayout_23.addWidget(self.storeSelSetup_Btn, 0, 0, 1, 2)
        self.gridLayout_24.addLayout(self.gridLayout_23, 0, 0, 1, 1)
        self.gridLayout_46.addWidget(self.groupBox_10, 1, 0, 1, 1)
        self.groupBox_8 = QtWidgets.QGroupBox(self.zToolsTab)
        self.groupBox_8.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color: rgb(25, 145, 255);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color: rgb(25, 145, 255);\n"
"}")
        self.groupBox_8.setObjectName("groupBox_8")
        self.gridLayout_45 = QtWidgets.QGridLayout(self.groupBox_8)
        self.gridLayout_45.setSpacing(3)
        self.gridLayout_45.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_45.setObjectName("gridLayout_45")
        self.gridLayout_26 = QtWidgets.QGridLayout()
        self.gridLayout_26.setSpacing(3)
        self.gridLayout_26.setObjectName("gridLayout_26")
        self.gridLayout_25 = QtWidgets.QGridLayout()
        self.gridLayout_25.setSpacing(3)
        self.gridLayout_25.setObjectName("gridLayout_25")
        self.gridLayout_11 = QtWidgets.QGridLayout()
        self.gridLayout_11.setSpacing(3)
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.label_8 = QtWidgets.QLabel(self.groupBox_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy)
        self.label_8.setMinimumSize(QtCore.QSize(0, 15))
        self.label_8.setObjectName("label_8")
        self.gridLayout_11.addWidget(self.label_8, 0, 0, 1, 1)
        self.boneBlendSuffix_Txt = QtWidgets.QLineEdit(self.groupBox_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.boneBlendSuffix_Txt.sizePolicy().hasHeightForWidth())
        self.boneBlendSuffix_Txt.setSizePolicy(sizePolicy)
        self.boneBlendSuffix_Txt.setPlaceholderText("")
        self.boneBlendSuffix_Txt.setObjectName("boneBlendSuffix_Txt")
        self.gridLayout_11.addWidget(self.boneBlendSuffix_Txt, 1, 0, 1, 1)
        self.gridLayout_25.addLayout(self.gridLayout_11, 0, 0, 1, 1)
        self.addSuffix_Btn = QtWidgets.QPushButton(self.groupBox_8)
        self.addSuffix_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.addSuffix_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(22, 162, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(26, 188, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.addSuffix_Btn.setObjectName("addSuffix_Btn")
        self.gridLayout_25.addWidget(self.addSuffix_Btn, 1, 0, 1, 1)
        self.removeSuffix_Btn = QtWidgets.QPushButton(self.groupBox_8)
        self.removeSuffix_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.removeSuffix_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(24, 155, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(27, 181, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.removeSuffix_Btn.setObjectName("removeSuffix_Btn")
        self.gridLayout_25.addWidget(self.removeSuffix_Btn, 2, 0, 1, 1)
        self.gridLayout_26.addLayout(self.gridLayout_25, 0, 0, 1, 1)
        self.line = QtWidgets.QFrame(self.groupBox_8)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout_26.addWidget(self.line, 0, 1, 1, 1)
        self.gridLayout_10 = QtWidgets.QGridLayout()
        self.gridLayout_10.setSpacing(3)
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.gridLayout_9 = QtWidgets.QGridLayout()
        self.gridLayout_9.setSpacing(3)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.boneBlendOff_Spn = QtWidgets.QSpinBox(self.groupBox_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.boneBlendOff_Spn.sizePolicy().hasHeightForWidth())
        self.boneBlendOff_Spn.setSizePolicy(sizePolicy)
        self.boneBlendOff_Spn.setMinimum(-100)
        self.boneBlendOff_Spn.setProperty("value", 0)
        self.boneBlendOff_Spn.setObjectName("boneBlendOff_Spn")
        self.gridLayout_9.addWidget(self.boneBlendOff_Spn, 1, 0, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.groupBox_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)
        self.label_7.setMinimumSize(QtCore.QSize(0, 15))
        self.label_7.setObjectName("label_7")
        self.gridLayout_9.addWidget(self.label_7, 0, 1, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.groupBox_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)
        self.label_6.setMinimumSize(QtCore.QSize(0, 15))
        self.label_6.setObjectName("label_6")
        self.gridLayout_9.addWidget(self.label_6, 0, 0, 1, 1)
        self.boneBlendOn_Spn = QtWidgets.QSpinBox(self.groupBox_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.boneBlendOn_Spn.sizePolicy().hasHeightForWidth())
        self.boneBlendOn_Spn.setSizePolicy(sizePolicy)
        self.boneBlendOn_Spn.setMinimum(-100)
        self.boneBlendOn_Spn.setProperty("value", 1)
        self.boneBlendOn_Spn.setObjectName("boneBlendOn_Spn")
        self.gridLayout_9.addWidget(self.boneBlendOn_Spn, 1, 1, 1, 1)
        self.gridLayout_10.addLayout(self.gridLayout_9, 0, 0, 1, 1)
        self.boneBlendshape_Btn = QtWidgets.QPushButton(self.groupBox_8)
        self.boneBlendshape_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.boneBlendshape_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(22, 162, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(26, 188, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.boneBlendshape_Btn.setObjectName("boneBlendshape_Btn")
        self.gridLayout_10.addWidget(self.boneBlendshape_Btn, 1, 0, 1, 1)
        self.boneBlendDel_Btn = QtWidgets.QPushButton(self.groupBox_8)
        self.boneBlendDel_Btn.setMinimumSize(QtCore.QSize(120, 20))
        self.boneBlendDel_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(24, 155, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(27, 181, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.boneBlendDel_Btn.setObjectName("boneBlendDel_Btn")
        self.gridLayout_10.addWidget(self.boneBlendDel_Btn, 2, 0, 1, 1)
        self.gridLayout_26.addLayout(self.gridLayout_10, 0, 2, 1, 1)
        self.gridLayout_45.addLayout(self.gridLayout_26, 0, 0, 1, 1)
        self.gridLayout_31 = QtWidgets.QGridLayout()
        self.gridLayout_31.setSpacing(3)
        self.gridLayout_31.setObjectName("gridLayout_31")
        self.createWrap_Btn = QtWidgets.QPushButton(self.groupBox_8)
        self.createWrap_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.createWrap_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(25, 146, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(28, 172, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.createWrap_Btn.setObjectName("createWrap_Btn")
        self.gridLayout_31.addWidget(self.createWrap_Btn, 1, 0, 1, 1)
        self.deleteWrap_Btn = QtWidgets.QPushButton(self.groupBox_8)
        self.deleteWrap_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.deleteWrap_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(25, 146, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(28, 172, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.deleteWrap_Btn.setObjectName("deleteWrap_Btn")
        self.gridLayout_31.addWidget(self.deleteWrap_Btn, 1, 1, 1, 1)
        self.line_5 = QtWidgets.QFrame(self.groupBox_8)
        self.line_5.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.gridLayout_31.addWidget(self.line_5, 0, 0, 1, 2)
        self.gridLayout_45.addLayout(self.gridLayout_31, 1, 0, 1, 1)
        self.gridLayout_46.addWidget(self.groupBox_8, 2, 0, 1, 1)
        self.groupBox_19 = QtWidgets.QGroupBox(self.zToolsTab)
        self.groupBox_19.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color: rgb(34, 87, 255);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color: rgb(34, 87, 255);\n"
"}")
        self.groupBox_19.setObjectName("groupBox_19")
        self.gridLayout_50 = QtWidgets.QGridLayout(self.groupBox_19)
        self.gridLayout_50.setSpacing(3)
        self.gridLayout_50.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_50.setObjectName("gridLayout_50")
        self.gridLayout_47 = QtWidgets.QGridLayout()
        self.gridLayout_47.setSpacing(3)
        self.gridLayout_47.setObjectName("gridLayout_47")
        self.label_9 = QtWidgets.QLabel(self.groupBox_19)
        self.label_9.setObjectName("label_9")
        self.gridLayout_47.addWidget(self.label_9, 0, 0, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.groupBox_19)
        self.label_10.setObjectName("label_10")
        self.gridLayout_47.addWidget(self.label_10, 0, 1, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.groupBox_19)
        self.label_11.setObjectName("label_11")
        self.gridLayout_47.addWidget(self.label_11, 0, 2, 1, 1)
        self.keyOffFrame_Spn = QtWidgets.QSpinBox(self.groupBox_19)
        self.keyOffFrame_Spn.setMinimum(-99)
        self.keyOffFrame_Spn.setProperty("value", 0)
        self.keyOffFrame_Spn.setObjectName("keyOffFrame_Spn")
        self.gridLayout_47.addWidget(self.keyOffFrame_Spn, 1, 0, 1, 1)
        self.keyOnFrame_Spn = QtWidgets.QSpinBox(self.groupBox_19)
        self.keyOnFrame_Spn.setMinimum(-98)
        self.keyOnFrame_Spn.setProperty("value", 4)
        self.keyOnFrame_Spn.setObjectName("keyOnFrame_Spn")
        self.gridLayout_47.addWidget(self.keyOnFrame_Spn, 1, 1, 1, 1)
        self.keyDamp_Spn = QtWidgets.QSpinBox(self.groupBox_19)
        self.keyDamp_Spn.setProperty("value", 0)
        self.keyDamp_Spn.setObjectName("keyDamp_Spn")
        self.gridLayout_47.addWidget(self.keyDamp_Spn, 1, 2, 1, 1)
        self.keyRunUp_Btn = QtWidgets.QPushButton(self.groupBox_19)
        self.keyRunUp_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.keyRunUp_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(29, 121, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(34, 142, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.keyRunUp_Btn.setObjectName("keyRunUp_Btn")
        self.gridLayout_47.addWidget(self.keyRunUp_Btn, 2, 0, 1, 3)
        self.gridLayout_50.addLayout(self.gridLayout_47, 0, 0, 1, 1)
        self.gridLayout_46.addWidget(self.groupBox_19, 3, 0, 1, 1)
        self.groupBox_13 = QtWidgets.QGroupBox(self.zToolsTab)
        self.groupBox_13.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color: rgb(30, 114, 255);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color: rgb(30, 114, 255);\n"
"}")
        self.groupBox_13.setObjectName("groupBox_13")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.groupBox_13)
        self.gridLayout_8.setSpacing(3)
        self.gridLayout_8.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setSpacing(3)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.multiObjImp_Btn = QtWidgets.QPushButton(self.groupBox_13)
        self.multiObjImp_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.multiObjImp_Btn.setStatusTip("")
        self.multiObjImp_Btn.setWhatsThis("")
        self.multiObjImp_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(31, 109, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(36, 126, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.multiObjImp_Btn.setObjectName("multiObjImp_Btn")
        self.gridLayout_2.addWidget(self.multiObjImp_Btn, 0, 0, 1, 1)
        self.multiObjExp_Btn = QtWidgets.QPushButton(self.groupBox_13)
        self.multiObjExp_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.multiObjExp_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(32, 103, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(37, 121, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.multiObjExp_Btn.setObjectName("multiObjExp_Btn")
        self.gridLayout_2.addWidget(self.multiObjExp_Btn, 1, 0, 1, 1)
        self.gridLayout_8.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        self.gridLayout_46.addWidget(self.groupBox_13, 4, 0, 1, 1)
        self.groupBox_9 = QtWidgets.QGroupBox(self.zToolsTab)
        self.groupBox_9.setStyleSheet("QGroupBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 9px;\n"
"    margin-top: 0.5em;\n"
"    border-color: rgb(34, 87, 255);\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 10px;\n"
"    padding: 0 3px 0 3px;\n"
"    color: rgb(34, 87, 255);\n"
"}")
        self.groupBox_9.setObjectName("groupBox_9")
        self.gridLayout_42 = QtWidgets.QGridLayout(self.groupBox_9)
        self.gridLayout_42.setSpacing(3)
        self.gridLayout_42.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_42.setObjectName("gridLayout_42")
        self.gridLayout_38 = QtWidgets.QGridLayout()
        self.gridLayout_38.setSpacing(3)
        self.gridLayout_38.setObjectName("gridLayout_38")
        self.matchPointPos_Btn = QtWidgets.QPushButton(self.groupBox_9)
        self.matchPointPos_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.matchPointPos_Btn.setStatusTip("")
        self.matchPointPos_Btn.setWhatsThis("")
        self.matchPointPos_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(34, 92, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(40, 108, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.matchPointPos_Btn.setObjectName("matchPointPos_Btn")
        self.gridLayout_38.addWidget(self.matchPointPos_Btn, 0, 0, 1, 1)
        self.cometRename_Btn = QtWidgets.QPushButton(self.groupBox_9)
        self.cometRename_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.cometRename_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(34, 87, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(40, 101, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.cometRename_Btn.setObjectName("cometRename_Btn")
        self.gridLayout_38.addWidget(self.cometRename_Btn, 1, 0, 1, 1)
        self.capitalize_Btn = QtWidgets.QPushButton(self.groupBox_9)
        self.capitalize_Btn.setMinimumSize(QtCore.QSize(0, 20))
        self.capitalize_Btn.setStyleSheet("QPushButton{\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(35, 82, 255);\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"QPushButton:hover {   \n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 4px;\n"
"background-color: rgb(41, 95, 255);\n"
"color: rgb(0, 0, 0);\n"
"}")
        self.capitalize_Btn.setObjectName("capitalize_Btn")
        self.gridLayout_38.addWidget(self.capitalize_Btn, 2, 0, 1, 1)
        self.gridLayout_42.addLayout(self.gridLayout_38, 0, 0, 1, 1)
        self.gridLayout_46.addWidget(self.groupBox_9, 5, 0, 1, 1)
        spacerItem7 = QtWidgets.QSpacerItem(264, 13, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_46.addItem(spacerItem7, 6, 0, 1, 1)
        self.gridLayout_52.addLayout(self.gridLayout_46, 0, 0, 1, 1)
        self.UsrTet_Spn.addTab(self.zToolsTab, "")
        self.gridLayout_51.addWidget(self.UsrTet_Spn, 0, 0, 1, 1)
        zivaWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(zivaWindow)
        self.UsrTet_Spn.setCurrentIndex(0)
        self.selectAll_Combo.setCurrentIndex(3)
        QtCore.QMetaObject.connectSlotsByName(zivaWindow)

    def retranslateUi(self, zivaWindow):
        zivaWindow.setWindowTitle(QtWidgets.QApplication.translate("zivaWindow", "cmZiva", None, -1))
        self.groupBox.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Add", None, -1))
        self.addBone_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Turns selected mesh into zBone", None, -1))
        self.addBone_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Bone", None, -1))
        self.addTissue_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Turns selected mesh into zTissue", None, -1))
        self.addTissue_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Tissue", None, -1))
        self.addFiber_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Adds zFiber to selected zTissue", None, -1))
        self.addFiber_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Fiber", None, -1))
        self.addAttachment_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Creates attachment between two selected zObjects", None, -1))
        self.addAttachment_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Attachment", None, -1))
        self.addCloth_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Turns selected mesh into zCloth", None, -1))
        self.addCloth_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Cloth", None, -1))
        self.addzCache_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Adds cache node to zSolver", None, -1))
        self.addzCache_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "zCache", None, -1))
        self.addSolver_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Adds solver to scene", None, -1))
        self.addSolver_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Solver", None, -1))
        self.selectProximity_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Selects vertex within a defined distance from second selection", None, -1))
        self.selectProximity_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Select By Proximity", None, -1))
        self.groupBox_3.setTitle(QtWidgets.QApplication.translate("zivaWindow", "zTet", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("zivaWindow", " zTet Size", None, -1))
        self.tetSize_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Tet Size on Tissue creation", None, -1))
        self.tetSize_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Pre", None, -1))
        self.usrTetSize_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Tet Size on selected zTissue(s)", None, -1))
        self.usrTetSize_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Post", None, -1))
        self.groupBox_4.setTitle(QtWidgets.QApplication.translate("zivaWindow", "zTissue", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("zivaWindow", "Contact Stiffness", None, -1))
        self.contactStiffness_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Contact Stiffness on Tissue creation", None, -1))
        self.contactStiffness_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Pre", None, -1))
        self.usrContactStiff_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Contact Stiffness on selected zTissue(s)", None, -1))
        self.usrContactStiff_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Post", None, -1))
        self.label_3.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Compression Resistance", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("zivaWindow", "Compress Res.", None, -1))
        self.compressionResistance_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Compression Resistance on Tissue creation", None, -1))
        self.compressionResistance_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Pre", None, -1))
        self.usrCompRes_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Compression Resistance on selected zTissue(s)", None, -1))
        self.usrCompRes_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Post", None, -1))
        self.groupBox_5.setTitle(QtWidgets.QApplication.translate("zivaWindow", "zMaterial", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("zivaWindow", "Poissons Ratio", None, -1))
        self.poissonsRatio_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Poissons Ratio on Tissue creation", None, -1))
        self.poissonsRatio_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Pre", None, -1))
        self.usrPoisRatio_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Poissons Ratio on selected zTissue(s)", None, -1))
        self.usrPoisRatio_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Post", None, -1))
        self.label_5.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Volume Conservation", None, -1))
        self.label_5.setText(QtWidgets.QApplication.translate("zivaWindow", "Volume Cons.", None, -1))
        self.volumeCons_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Volume Conservation on Tissue creation", None, -1))
        self.volumeCons_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Pre", None, -1))
        self.usrVolumeCons_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets Volume Conservation on selected zTissue(s)", None, -1))
        self.usrVolumeCons_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Post", None, -1))
        self.groupBox_18.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Select Multi", None, -1))
        self.selectAll_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Select", None, -1))
        self.label_13.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Selects all, unless items are currently selected.", None, -1))
        self.label_13.setText(QtWidgets.QApplication.translate("zivaWindow", "Select All", None, -1))
        self.selectAll_Combo.setItemText(0, QtWidgets.QApplication.translate("zivaWindow", "zBones", None, -1))
        self.selectAll_Combo.setItemText(1, QtWidgets.QApplication.translate("zivaWindow", "zTissues", None, -1))
        self.selectAll_Combo.setItemText(2, QtWidgets.QApplication.translate("zivaWindow", "zMaterials", None, -1))
        self.selectAll_Combo.setItemText(3, QtWidgets.QApplication.translate("zivaWindow", "zFibers", None, -1))
        self.selectAll_Combo.setItemText(4, QtWidgets.QApplication.translate("zivaWindow", "zAttachments", None, -1))
        self.groupBox_11.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Tools", None, -1))
        self.computeVol_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Compute Volume", None, -1))
        self.toggleMuscle_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Turns zTissue on/off", None, -1))
        self.toggleMuscle_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Toggle Muscle(s)", None, -1))
        self.renameNodes_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Rename Nodes", None, -1))
        self.zBuilderWindow_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Opens zBuilder", None, -1))
        self.zBuilderWindow_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "zBuilder", None, -1))
        self.mirrorMuscleLR_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Mirrors all muscles L to R. Excludes \'_M_\' middle muscles", None, -1))
        self.mirrorMuscleLR_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "<-- Mirror ALL Muscles L to R -->", None, -1))
        self.meshCheck_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Mesh Check", None, -1))
        self.findIntersections_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Find Intersecting Geo", None, -1))
        self.groupBox_23.setTitle(QtWidgets.QApplication.translate("zivaWindow", "zCache Save", None, -1))
        self.StartFrame.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Selects all, unless items are currently selected.", None, -1))
        self.StartFrame.setText(QtWidgets.QApplication.translate("zivaWindow", "Start Frame", None, -1))
        self.EndFrame.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Selects all, unless items are currently selected.", None, -1))
        self.EndFrame.setText(QtWidgets.QApplication.translate("zivaWindow", "End Frame", None, -1))
        self.saveCache_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Save zCache", None, -1))
        self.saveCache_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Save Cache", None, -1))
        self.groupBox_24.setTitle(QtWidgets.QApplication.translate("zivaWindow", "zCache Load", None, -1))
        self.StartFrame_2.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Selects all, unless items are currently selected.", None, -1))
        self.StartFrame_2.setText(QtWidgets.QApplication.translate("zivaWindow", "Start Frame", None, -1))
        self.EndFrame_2.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Selects all, unless items are currently selected.", None, -1))
        self.EndFrame_2.setText(QtWidgets.QApplication.translate("zivaWindow", "End Frame", None, -1))
        self.loadCache_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Loads zCache", None, -1))
        self.loadCache_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Load Cache", None, -1))
        self.groupBox_2.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Remove", None, -1))
        self.delAttachment_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Attachments", None, -1))
        self.delFiber_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Fiber", None, -1))
        self.delzCache_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "zCache", None, -1))
        self.clearSelected_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Clear Selected", None, -1))
        self.clearScene_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Clear Scene", None, -1))
        self.clearCache_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Clear Cache", None, -1))
        self.UsrTet_Spn.setTabText(self.UsrTet_Spn.indexOf(self.zSetupTab), QtWidgets.QApplication.translate("zivaWindow", "zSetup", None, -1))
        self.groupBox_16.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Create LOA", None, -1))
        self.createLOA_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Creates LOA Curve based on selected muscle zFiber direction. Select muscle and run.", None, -1))
        self.createLOA_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Create LOA curve", None, -1))
        self.clusterOnCurve_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Creates clusters on each CV point for selected curve. Rebuilds clusters if they exist", None, -1))
        self.clusterOnCurve_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Cluster On Curve Point(s)", None, -1))
        self.clusterOnCV_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Creates cluster on selected CV Point", None, -1))
        self.clusterOnCV_Btn.setStatusTip(QtWidgets.QApplication.translate("zivaWindow", "Toggles LOA curves vis On/Off", None, -1))
        self.clusterOnCV_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Cluster On Curve Point", None, -1))
        self.LOARivet_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Rivets clusters to mesh objects. Select cluster, then mesh, and run.", None, -1))
        self.LOARivet_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Rivet Cluster to Bone", None, -1))
        self.LOAAutoRivet_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "If curve cluster0 and cluster1 parent attrs are populated, this function will re-rivet the curve to cluster parents", None, -1))
        self.LOAAutoRivet_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Auto RE-Rivet Curve(s)", None, -1))
        self.groupBox_12.setTitle(QtWidgets.QApplication.translate("zivaWindow", "LOA Setup Custom", None, -1))
        self.loaFlexion_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "When LOA curve shortens in length", None, -1))
        self.loaFlexion_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Flexion", None, -1))
        self.curveFlexionLength_Spn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Length of LOA curve when muscle is at flex position", None, -1))
        self.LOACurrentLengthFlexion_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Gets length of selected LOA Curve and populates Flexion field", None, -1))
        self.LOACurrentLengthFlexion_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Get LOA Len.", None, -1))
        self.loaExtension_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "When LOA curve grows in length", None, -1))
        self.loaExtension_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Extension", None, -1))
        self.curveExtensionLength_Spn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Length of LOA curve when muscle is at extended position", None, -1))
        self.LOACurrentLengthExtension_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Gets length of selected LOA Curve and populates Extension field", None, -1))
        self.LOACurrentLengthExtension_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Get LOA Len.", None, -1))
        self.label_12.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Values higher then 1.0 will increast muscle size, triggered by LOA Excitation.", None, -1))
        self.label_12.setText(QtWidgets.QApplication.translate("zivaWindow", "Muscle Growth", None, -1))
        self.muscleGrowth_Spn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Length of LOA curve when muscle is at extended position", None, -1))
        self.LOAMuscleCustom_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Creates custom LOA setup for Flexion, Extension and Muscle Growth. Select LOA curve and Muscle and Run. (Make sure LOA is in default pose before running)", None, -1))
        self.LOAMuscleCustom_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Create Custom LOA Setup", None, -1))
        self.LOAMuscleCustomReconnect_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Reconnects custom LOA nodes to muscle Fibers. Select all muscles and run. Used after updating muscle geo.", None, -1))
        self.LOAMuscleCustomReconnect_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Re-Connect Custom LOA Nodes(s)", None, -1))
        self.LOAMuscleCustomDel_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Removes custom LOA setup and associated nodes for selected Fiber. Select Muscle and run.", None, -1))
        self.LOAMuscleCustomDel_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Delete Custom LOA Setup", None, -1))
        self.groupBox_15.setTitle(QtWidgets.QApplication.translate("zivaWindow", "LOA Setup Ziva", None, -1))
        self.LOAMuscle_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Sets up zLineOfAction. Select LOA Curve, then Muscle, and run.", None, -1))
        self.LOAMuscle_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Create Ziva LOA Setup", None, -1))
        self.LOADelete_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Deletes Ziva zLineOfAction. Select Muscle and run.", None, -1))
        self.LOADelete_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Delete Ziva LOA Setup", None, -1))
        self.groupBox_17.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Un-parent / Re-parent", None, -1))
        self.LOAUnParent_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Moves LOA curves to scene root and removes clusters. Use Write Out to save cluster settings. Used when updating muscle geo.", None, -1))
        self.LOAUnParent_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "LOA Un-Parent", None, -1))
        self.writeOut_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Writes out parent dictionary to Maya\'s current working directory", None, -1))
        self.writeOut_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Write Out", None, -1))
        self.LOAReParent_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Re-parents LOA curves back to muscle parent. Use Read In to get parent settings from file.", None, -1))
        self.LOAReParent_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "LOA Re-Parent", None, -1))
        self.readIn_Chk.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Reads in parent dictionary to Maya\'s current working directory", None, -1))
        self.readIn_Chk.setText(QtWidgets.QApplication.translate("zivaWindow", "Read In", None, -1))
        self.groupBox_20.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Backup / Restore", None, -1))
        self.LOABackup_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Export LOA curves and zFiber connections to json", None, -1))
        self.LOABackup_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Backup LOA Curves", None, -1))
        self.LOARestore_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Creates LOA curves and associated zFiber connections from json", None, -1))
        self.LOARestore_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Restore LOA Curves", None, -1))
        self.toggleLOACurve_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Toggles LOA curves vis On/Off", None, -1))
        self.toggleLOACurve_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Toggle LOA Curve(s) Visibility", None, -1))
        self.UsrTet_Spn.setTabText(self.UsrTet_Spn.indexOf(self.LOATab), QtWidgets.QApplication.translate("zivaWindow", "LOA", None, -1))
        self.groupBox_6.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Update Ziva Object", None, -1))
        self.browseMuscleGeoDir_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Geo Dir", None, -1))
        self.updateMuscleGeo_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Updates zTissue/zBone objects. Point to dir where new obj lives, and update.", None, -1))
        self.updateMuscleGeo_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Update Ziva Objects", None, -1))
        self.groupBox_10.setTitle(QtWidgets.QApplication.translate("zivaWindow", "zSave", None, -1))
        self.storeSceneSetup_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Backups ziva to *.ziva, & LOA curves to *.json", None, -1))
        self.storeSceneSetup_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Backup Ziva Scene", None, -1))
        self.retrieveSetup_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Restores *.ziva, & LOA Curves *.json (with same name) if it exists", None, -1))
        self.retrieveSetup_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Retrieve Ziva Setup", None, -1))
        self.storeSelSetup_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Backup Ziva Selection", None, -1))
        self.groupBox_8.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Bone Blendshape", None, -1))
        self.label_8.setText(QtWidgets.QApplication.translate("zivaWindow", "Driver Suffix", None, -1))
        self.boneBlendSuffix_Txt.setText(QtWidgets.QApplication.translate("zivaWindow", "ZivaDriver", None, -1))
        self.addSuffix_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Add suffix specified in text field to selected objects", None, -1))
        self.addSuffix_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Add Suffix", None, -1))
        self.removeSuffix_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Removes suffix specified in text field from selected objects", None, -1))
        self.removeSuffix_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Remove Suffix", None, -1))
        self.label_7.setText(QtWidgets.QApplication.translate("zivaWindow", "On Frame", None, -1))
        self.label_6.setText(QtWidgets.QApplication.translate("zivaWindow", "Off Frame", None, -1))
        self.boneBlendshape_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Select cache objects with \'Driver Suffix\' and run. Will look for matching object names -Driver Suffix, and blendshape them to cache.", None, -1))
        self.boneBlendshape_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Create Blendshape(s)", None, -1))
        self.boneBlendDel_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Select objects with blendshape(s) and run.", None, -1))
        self.boneBlendDel_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Delete Blendshape(s)", None, -1))
        self.createWrap_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "<html><head/><body><p>Matches objects to corresponding objects+suffix, and creates CVWrap to objects with suffix. OR wraps selected objects to matching objects+suffix</p></body></html>", None, -1))
        self.createWrap_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Create Wrap(s)", None, -1))
        self.deleteWrap_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "<html><head/><body><p>Deletes cvWrap(s) that were created with this tool from selected objects</p></body></html>", None, -1))
        self.deleteWrap_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Delete Wrap(s)", None, -1))
        self.groupBox_19.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Key Run up (Cloth / Tissue)", None, -1))
        self.label_9.setText(QtWidgets.QApplication.translate("zivaWindow", "Off Frame", None, -1))
        self.label_10.setText(QtWidgets.QApplication.translate("zivaWindow", "On Frame", None, -1))
        self.label_11.setText(QtWidgets.QApplication.translate("zivaWindow", "Damp Padding", None, -1))
        self.keyRunUp_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Key Run Up", None, -1))
        self.groupBox_13.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Multi Obj Import / Export", None, -1))
        self.multiObjImp_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Imports all OBJ\'s in specified directory. Also does rename", None, -1))
        self.multiObjImp_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Multi Obj Import", None, -1))
        self.multiObjExp_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Muli Obj Export", None, -1))
        self.groupBox_9.setTitle(QtWidgets.QApplication.translate("zivaWindow", "Miscellaneous", None, -1))
        self.matchPointPos_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "Select NEW shape, Then OLD shape and run", None, -1))
        self.matchPointPos_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Match Point Pos", None, -1))
        self.cometRename_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Comet Rename", None, -1))
        self.capitalize_Btn.setToolTip(QtWidgets.QApplication.translate("zivaWindow", "<html><head/><body><p>Capitalizes the beginning of each word separated by &quot;_&quot;</p></body></html>", None, -1))
        self.capitalize_Btn.setText(QtWidgets.QApplication.translate("zivaWindow", "Capitalize", None, -1))
        self.UsrTet_Spn.setTabText(self.UsrTet_Spn.indexOf(self.zToolsTab), QtWidgets.QApplication.translate("zivaWindow", "zTools", None, -1))

