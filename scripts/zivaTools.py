import maya.cmds as cmds
import maya.mel as mm
import maya.OpenMaya as om
from pymel import util
import pymel.core as pm
from functools import partial
from collections import OrderedDict
from ast import literal_eval
import json
import os
import re
import omUtil as omu
import rigUtils as rigU
import mgear.core.transform as tra
import zBuilder.builders.ziva as zva
import zBuilder.zMaya as mz
import importlib
importlib.reload(mz)
import zBuilder.ui.zUI as zb
import logging
logger = logging.getLogger(__name__)



def addFiber(muscle=None):
    # Requires object to be selected, muscle var comes from LOAMuscleCustom
    if muscle:
        cmds.select(muscle)
        zFiber = mm.eval("ziva -f")
        renameNodes()

    else:
        fiberObjs = cmds.ls(sl=True)
        if fiberObjs:
            for obj in fiberObjs:
                findFiber = None
                findFiber = mm.eval('zQuery -t "zFiber" {0}'.format(obj))# Check for existing fiber
                if findFiber:
                    confirm = cmds.confirmDialog(message = 'Fiber already Exists, create another zFiber?', button=['Yes','No'])
                    if confirm == "Yes":
                        cmds.select(obj)
                        mm.eval("ziva -f")
                        renameNodes()
                    if confirm == 'No':
                        continue
                else:
                    cmds.select(obj)
                    mm.eval("ziva -f")
                    renameNodes()

def renameNodes():
    mz.rename_ziva_nodes()

def mirrorMuscleLR():
    '''
    TODO: Save ziva LOA setup. Currently only saves custom LOA
    '''
    muscleMirrorGrp = []
    musBuildGrp = {}
    
    # Mirror selected muscles
    if cmds.ls(sl=1):
        selGeo = cmds.ls(sl=1)
        for s in selGeo:
            if cmds.nodeType(cmds.listConnections(s+'.worldMatrix[0]')[0]) == 'zGeo':
                muscleMirrorGrp.append(s)

    # Mirror all muscles
    else: 
        zGeoNodes = mm.eval('zQuery -t "zGeo"')
        for zGeo in zGeoNodes:
            if cmds.listConnections(zGeo+'.oGeo', type='zTissue'):
                if '_M_' not in str(cmds.listConnections(zGeo+'.iNeutralMatrix')):
                    muscleMirrorGrp.append(cmds.listConnections(zGeo+'.iNeutralMatrix')[0])
    
    if muscleMirrorGrp == []:
        raise IndexError('No tissues found, or selected for mirror')


    # Remove middle tissues
    [muscleMirrorGrp.remove(node) for node in muscleMirrorGrp if '_M_' in node]



    # Save custom, and ziva LOA settings for muscleMirrorGrp
    fiberGrp  = []
    customLOA = []
    zivaLOA   = []
    
    # Determine which items have a zFiber
    for mus in muscleMirrorGrp:
        if mm.eval('zQuery -t "zFiber" {nde}'.format(nde=mus)):
            fiberGrp.append(mm.eval('zQuery -t "zFiber" {nde}'.format(nde=mus))[0])
        
    # Determine if using ziva LOA or custom LOA
    if fiberGrp != []:
        print('Fibers to mirror : ', '<'*80)
        for f in fiberGrp:
            # Check for 'zLineOfAction' (ziva LOA)
            if cmds.nodeType(cmds.listConnections(f+'.iLineOfActionData'))=='zLineOfAction':
                zivaLOA.append(cmds.listConnections(f+'.iLineOfActionData')[0])
            
            else: # Check for 'curveInfo' node (custom LOA)
                print(f, '<'*(80-len(f))) # print fiber name
                if cmds.listConnections(f+'.muscleGrowthScale') != None: # This attr is always driven by custom loa setup
                    remap = cmds.listConnections(f+'.muscleGrowthScale')[0] # muscleGrowthScale is driven by remap node
                    if cmds.nodeType(remap) == 'remapValue':
                        if cmds.listConnections(remap+'.outputMax') != []: # Check for 'curveInfo' node for custom loa settings
                            if cmds.nodeType(cmds.listConnections(remap+'.outputMax')) == 'curveInfo':
                                customLOA.append(cmds.listConnections(remap+'.outputMax')[0]) # Append curveInfo node
                else:
                    raise AttributeError('There is a fiber without a LOA connected. Check the last print')


    # Store custom LOA settings
    loaCustSet = None
    if customLOA != []:
        print('Custom LOA "curveInfo" nodes to extract settings from : ', customLOA)
        loaCustSet = loaCustomSettings(curves=True, curveInfoNodes=customLOA)
        print('curveInfo node settings : ', loaCustSet)
    
    '''
    # Store ziva LOA settings
    loaZivaSet = None
    if zivaLOA != []:
        loaZivaSet = loaZivaSettings(zLineOfActionNodes=zivaLOA)
    '''


    # Store muscle setup
    cmds.select(muscleMirrorGrp)
    z = zva.Ziva()
    z.retrieve_from_scene_selection()


    # Mirror
    for m in muscleMirrorGrp:
        meshTissue = mm.eval('zQuery -t "zTissue" {0}'.format (m)) # Get zTissue for run up anim keys
        musDup = cmds.duplicate(m, name=convertRLName(m), rc=True)
       
        # Mirror muscle
        traNode=pm.PyNode(musDup[0])
        rigU.lockUnlockSRT2(musDup[0], 1, 0)
        t = tra.getTransform(traNode)
        t = tra.getSymmetricalTransform(t)
        tra.setTransformFromMatrix(t, traNode)
        pm.makeIdentity(traNode, apply=True, t=1, r=1, s=1, n=0, pn=1)
        musBuildGrp[meshTissue[0]] = musDup[0] # Add to dict for copy/paste anim

        if cmds.listRelatives(musDup[0], c=True, type='transform'): # Rename children if they exist
            for item in cmds.listRelatives(musDup[0], ad=True):
                cmds.rename(item, convertRLName(item)[:-1]) # Rename

            for item in cmds.listRelatives(musDup[0], c=True):# Check for LOA curve and rename cluster parent attrs
                if item.endswith('_LOA_Curve'):
                    if cmds.objectType(omu.getDagPath(item, shape=True))=='nurbsCurve':
                        cluster0Par = cmds.setAttr(item+'.cluster0parent', convertRLName(cmds.getAttr(item+'.cluster0parent')), type='string')
                        cluster1Par = cmds.setAttr(item+'.cluster1parent', convertRLName(cmds.getAttr(item+'.cluster1parent')), type='string')
                        newClusters = clusterOnCurve(curve=item)
                        loaRivet([newClusters[0], cmds.getAttr(item+'.cluster0parent')])
                        loaRivet([newClusters[1], cmds.getAttr(item+'.cluster1parent')])

    # Build the muscles
    cmds.select(None) 
    z.string_replace('_L_','_R_')
    z.build()

    # Copy anim on attrs from L to R
    animAttrs = ['inertialDamping', 'restScaleEnvelope', 'pressureEnvelope', 'surfaceTensionEnvelope']
    for key, value in musBuildGrp.items():
        value = mm.eval('zQuery -t "zTissue" {0}'.format (value))
        if value:
            for attr in animAttrs:
                if cmds.keyframe(key, attribute=attr, query=True, keyframeCount=True ) > 0:
                    cmds.copyKey(key, attribute = attr)
                    cmds.pasteKey(value, attribute = attr)


    # Build custom LOA settings
    if loaCustSet != None:
        buildCustom = []
        for key, value in loaCustSet.items():
            if key[0] == 'loaNodeBkup':
                inputCrv = key[1]
                zFiber = key[2]
                muscle = key[2][:-8]
                growthScale = list(value.items())[0][1] # growthScale
                loaFlexionChk = list(value.items())[1][1] # loaFlexionChk
                loaExtensionChk = list(value.items())[2][1] # loaExtensionChk
                flexionValue = list(value.items())[3][1] # flexionValue
                extensionValue = list(value.items())[4][1] # extensionValue
                buildCustom.append([flexionValue[0], extensionValue[0], growthScale[0], loaFlexionChk[0], loaExtensionChk[0], convertRLName(inputCrv), convertRLName(zFiber), convertRLName(muscle)])
            else:
                pass

        print('buildCustom : ', buildCustom, '*'*300)

        for build in buildCustom:
            loaMuscleCustom(*build) #  '*' will unpack list
    '''
    # Build Ziva LOA settings
    if loaZivaSet != None:
        for loaNode, attrlist in loaZivaSet.items():
            loaSplit = loaNode.split("_")
            loaSplit[1] = "R"
            loaNodeR = "_".join(loaSplit)
            if cmds.objExists (loaNodeR) : 
                for attr, value in attrlist :
                    if cmds.attributeQuery (attr, node = loaNodeR, exists=True) :
                        cmds.setAttr (loaNodeR+"."+attr, value)
    '''

def zBuilderWindow():
    zb.run()

def delSelFiber():
    fiberObjs = cmds.ls(sl=True)
    for obj in fiberObjs:
        # Check for multiple zFibers on muscle. Prompt user to select fiber if multiple.
        if len(mm.eval('zQuery -t "zFiber" {0}'.format(obj))) >1:
            cmds.select(obj)
            zFiber = cmds.layoutDialog(ui=singleFiberSelectDelFiber)
            if zFiber == 'dismiss':
                continue
            else:
                cmds.delete(zFiber)
        else:
            zFiber = mm.eval('zQuery -t "zFiber" {0}'.format(obj))
            if cmds.listConnections(zFiber, type='remapValue'): # Check for custom LOA setup on zFiber
                if 'LOACustomMuscleGrowth' in cmds.listConnections(zFiber, type='remapValue')[0]:
                    confirm = cmds.confirmDialog(message = 'This will delete the custom LOA setup on this zFiber, delete to continue?', button=['Yes','No'])
                    if confirm == "Yes":
                        cmds.delete(zFiber)
                    if confirm == 'No':
                        return
            else:
                cmds.delete(zFiber)

######################################## LOA Tab ########################################
def createLOA():
    # Create custom LOA curve with clusters
    if cmds.ls(sl=True):
        objList = cmds.ls(sl=True)
        for obj in objList:
            if (mm.eval('zQuery -t "zTissue" {0}'.format(obj))) and (mm.eval('zQuery -t "zFiber" {0}'.format(obj))): # Verify its a zTissue with zFiber
                if not cmds.objExists(str(obj)+'_LOA_Curve'): # Check for existing LOA curve
                    cmds.select(obj)
                    mm.eval("zLineOfActionUtil")
                    loaRename = cmds.rename(cmds.ls(sl=True), str(obj)+"_LOA_Curve")
                    cmds.parent(loaRename, obj)
                    cmds.setAttr(loaRename +'Shape.overrideEnabled', 1)
                    cmds.setAttr(loaRename +'Shape.overrideColor', 14)
                    # Create Clusters on curve control points. Number of CVs = degree + spans.
                    clusterSelectList = []
                    nbCVs = cmds.getAttr( loaRename +'.spans' ) + cmds.getAttr( loaRename +'.degree' )
                    for cvIndex in range (nbCVs):
                        theCV = "{0}.cv[{1}]".format (loaRename , cvIndex )
                        cvCluster = cmds.cluster(theCV, n=str(obj)+"_Cluster_0")
                        cmds.parent(cvCluster, loaRename)
                        clusterOnCurve(loaRename)
                else:
                    print (str(obj), '<<<<<<<------------------------------------------------------')
                    raise AttributeError('LOA curve already exists')
            else:
                print (str(obj), '<<<<<<<------------------------------------------------------')
                raise AttributeError('Not a zTissue, or no zFiber found')

def clusterOnCurve(curve=None):
    # Create Clusters on curve control points. Number of CVs = degree + spans.
    if curve:
        curveList = [curve]
    else:
        curveList = cmds.ls(sl=True)

    for curve in curveList:
        if cmds.objectType(omu.getDagPath(curve, shape=True)) != 'nurbsCurve': # Make sure nurbs curve
            continue
        if cmds.getAttr(curve +'.spans') + cmds.getAttr(curve +'.degree') != 2: # Only two cv's allowed
            cmds.warning('Curve has more then 2 CVs', str(curve))
            continue
        if 'cluster0parent' and 'cluster1parent' not in cmds.listAttr(curve): # Add clusterparent attr for rivet backup
            cmds.addAttr(curve, ci=True, dt="string", sn='cluster0parent')
            cmds.addAttr(curve, ci=True, dt="string", sn='cluster1parent')

        clusterCheck = cmds.listRelatives(curve, children=True, type='transform', fullPath=True)# If clusters exist, rebuild. Move to world. Freeze Curve. Make new clusters
        if clusterCheck != None:
            clusters=[]
            for item in clusterCheck:
                if item.endswith('_rivet'): # Make sure its rivet locator
                    cluster = cmds.rename(item, item +'_zivaTempCluster')
                    cmds.parent(cluster, world=True)
            cmds.delete(curve, constructionHistory=True) # Freeze curve history
            if cmds.objExists('*_zivaTempCluster'):
                cmds.delete('*_zivaTempCluster')
            # Make new clusters
            nbCVs = cmds.getAttr( curve +'.spans' ) + cmds.getAttr( curve +'.degree' )
            for cvIndex in range (nbCVs):
                theCV = "{0}.cv[{1}]".format (curve , cvIndex )
                cvCluster = cmds.cluster(theCV)
                renameCluster = cmds.rename(cvCluster[1], str(curve)+"_Cluster_"+str(cvIndex))
                cmds.parent(renameCluster, curve)
                clusters.append(renameCluster)
            if cmds.objExists('*_zivaTempCluster'):
                cmds.delete('*_zivaTempCluster')
            return clusters
        else:
            clusters=[]
            nbCVs = cmds.getAttr( curve +'.spans' ) + cmds.getAttr( curve +'.degree' )
            for cvIndex in range (nbCVs):
                theCV = "{0}.cv[{1}]".format (curve , cvIndex )
                cvCluster = cmds.cluster(theCV)
                renameCluster = cmds.rename(cvCluster[1], str(curve)+"_Cluster_"+str(cvIndex))
                cmds.parent(renameCluster, curve)
                clusters.append(renameCluster)

            return clusters

def clusterOnCV():
    # Creates clusters on selected CV
    cvParent = cmds.listRelatives(parent=True, shapes=False)
    clusterNameSplit = cvParent[0].split("_")
    clusterName = "_".join(clusterNameSplit[:4])
    theCV = cmds.ls(sl=True)[0]
    cvCluster = cmds.cluster(theCV)
    cmds.parent(cvCluster, cvParent)
    renameCluster = cmds.rename(cvCluster[1], str(clusterName)+"_Cluster_0")

def loaRivet(rivetObjs=None):
    if rivetObjs: # RivetObjs comes from mirrorMuscleLR(), or retrieveSetup(). [cluster, clusterParent]
        rivetObjs = rivetObjs
    else:
        rivetObjs = cmds.ls(sl=True)
    if len(rivetObjs) != 2:
        raise TypeError('Select cluster then ziva bone and run rivet again')

    if cmds.objectType(omu.getDagPath(rivetObjs[0], shape=True)) == 'clusterHandle':# Verify first obj is cluster
        clusterParent = cmds.listRelatives(rivetObjs[0], parent=True)[0]
        if cmds.objectType(omu.getDagPath(clusterParent, shape=True)) == 'nurbsCurve': # Verify clusters parents shape node is nurbscurve
            loaCrv = cmds.listRelatives(rivetObjs[0], parent=True)[0]
            auto_rivet([rivetObjs[0], rivetObjs[1]])
            cmds.setAttr(clusterParent+'.cluster'+rivetObjs[0][-1:]+'parent', str(rivetObjs[1]), type='string') # Store rivet parent for backup
        else:
            raise TypeError('Cluster is not a child of LOA Curve. Re-Rivet the curve')
    else:
        raise TypeError('First object is not LOA cluster')

def autoRivetCrv(loaCurve=None):
    if loaCurve: # loaCurve comes freom loaReParent
        loaCurves = [loaCurve]
    elif cmds.ls(sl=True):
        loaCurves = cmds.ls(sl=True)
    else:
        raise IndexError('Nothing selected')

    filteredCurves = []
    for i in loaCurves:
        if cmds.objectType(omu.getDagPath(i, shape=True))=='nurbsCurve':
            if '_LOA_Curve' in i:
                filteredCurves.append(i)
    if filteredCurves:
        for curve in filteredCurves:
            clusters = clusterOnCurve(curve)
            if cmds.getAttr(curve+'.cluster0parent'):
                loaRivet([clusters[0], cmds.getAttr(curve+'.cluster0parent')])
            if cmds.getAttr(curve+'.cluster1parent'):
                loaRivet([clusters[1], cmds.getAttr(curve+'.cluster1parent')])

def auto_rivet(everything_list): 
  '''
  TO DO: This script assumes you have rivet.mel in your scripts directory

  Description: 
    Given a list containing transforms and meshes, find the closest point on mesh from all 
    given meshes (so the closest of all closest points) And build a rivet on that mesh at 
    that point. Parent the transform under the rivet locator. 
  
  Accepts: 
    A list of transforms and meshes. 

  Returns: None
  
  '''

  lookupMeshes_dict = dict()
  transforms_list = list()

  for item in everything_list: 
    shapes = cmds.listRelatives( item, shapes=True )
    if shapes != None: 
      if cmds.objectType( shapes[0] ) == 'mesh': 
        
        mesh_mfnMesh = om.MFnMesh(omu.getDagPath( shapes[0], shape=False ) )
        lookupMeshes_dict[item] = mesh_mfnMesh
        
      else: 
        transforms_list.append(item)
  
  # ME
  finalParent = cmds.listRelatives(transforms_list[0], parent=True)

  for transform in transforms_list:
    closestDistance = 10e10
    closestMesh = None
    closestPolygon = None

    p = cmds.xform( transform, q=True, rp=True, ws=True, a=True )
    lookup_mPoint = om.MPoint( p[0], p[1], p[2] )
    lookup_mFloatPoint = om.MFloatPoint( lookup_mPoint.x, lookup_mPoint.y, lookup_mPoint.z )

    closestPolygon_mScriptUtil = om.MScriptUtil()

    for lookupMesh in lookupMeshes_dict.keys(): 
      currMesh_mfnMesh = lookupMeshes_dict[ lookupMesh ]
      
      closest_mPoint = om.MPoint()
      closest_mScriptUtil = om.MScriptUtil()
      closestPolygon_ptr = closest_mScriptUtil.asIntPtr()
      
      currMesh_mfnMesh.getClosestPoint( lookup_mPoint, closest_mPoint, om.MSpace.kWorld, closestPolygon_ptr )

      closestPolygon_currIndex = closest_mScriptUtil.getInt(closestPolygon_ptr)

      closest_mFloatPoint = om.MFloatPoint( closest_mPoint.x, closest_mPoint.y, closest_mPoint.z )

      d = closest_mFloatPoint.distanceTo( lookup_mFloatPoint )
      if d < closestDistance: 
        closestDistance = d
        closestMesh = lookupMesh
        closestPolygon = closestPolygon_currIndex

    prevIndex_util = om.MScriptUtil()

    meshName_mItMeshPolygon = om.MItMeshPolygon(omu.getDagPath( closestMesh, shape=False ) )
    meshName_mItMeshPolygon.setIndex( closestPolygon, prevIndex_util.asIntPtr() )

    edges_mIntArray = om.MIntArray()
    meshName_mItMeshPolygon.getEdges( edges_mIntArray )

    cmds.select( closestMesh+'.e[%i]' % edges_mIntArray[0], r=True )
    cmds.select( closestMesh+'.e[%i]' % edges_mIntArray[2], add=True )

    rivetLocator_str = mm.eval('rivet;')
    cmds.rename(cmds.listRelatives(rivetLocator_str, c=True, type='transform')[0], transform+'_AimConst')
    rivetLocator_str = cmds.rename( rivetLocator_str, transform+'_rivet' )

    cmds.parent( transform, rivetLocator_str )

    # ME
    cmds.parent (rivetLocator_str, finalParent)

def loaMuscle():
    # Creates Ziva LOA setup
    # Make sure LOA curve and muscle are in correct selection order (LOA Curve, then Muscle)
    currentSelection = cmds.ls(sl=True)
    if len(currentSelection) != 2:
        raise TypeError('Select LOA curve then muscle before using this function. Err1')

    if cmds.objectType(omu.getDagPath(currentSelection[0], shape=True)) != 'nurbsCurve': # Make sure nurbsCurve
        raise TypeError('Select LOA curve first, then muscle before using this function. Err2')

    if cmds.objectType(omu.getDagPath(currentSelection[1], shape=True)) == 'mesh': # Make sure mesh is a zTissue
        tissueShape = omu.getDagPath(currentSelection[1], shape=True)
        if 'zEmbedder' not in cmds.listConnections(tissueShape+'.inMesh')[0]:
            raise TypeError('Select LOA curve first, then muscle before using this function')

    inputCrv = currentSelection[0]
    selMuscle = currentSelection[1]

    # Make sure the muscle has a zFiber
    if not mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle)): # Check for existing fiber:
        confirm = cmds.confirmDialog(message = 'No zFiber on this muscle. Create it?', button=['Yes','No'])
        if confirm == 'Yes':
            addFiber(muscle=selMuscle)
            zFiber = mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle)) # Have to re-query as addFiber() renames nodes and cannot return node unless I switch to pm instead of cmds.
        if confirm == 'No':
            return

    # Check for multiple zFibers on muscle. Prompt user to select fiber if multiple.
    if len(mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle))) >1:
        raise AttributeError('Ziva LOA setup can only have one zFiber node. This is a limitation of Ziva. Use Remove Fiber to remove excess fiber nodes or Custom LOA setup for multiple fiber setups')
    else:
        zFiber = mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle))[0]

    # Make sure there is no Ziva LOA setup already.
    musLoa = cmds.listConnections(zFiber, type='zLineOfAction')
    if musLoa:
        confirm = cmds.confirmDialog(message = 'Ziva zLineOfAction already setup for this zFiber node. Delete it to continue?', button=['Yes','No'])
        if confirm == 'Yes':
            cmds.delete(musLoa)
        if confirm == 'No':
            return

    # Make sure there is no custom LOA setup already.
    if cmds.listConnections(zFiber, type='remapValue'):
        if 'LOACustomMuscleGrowth' in cmds.listConnections(zFiber, type='remapValue')[0]:
            confirm = cmds.confirmDialog(message = 'Custom LOA already exists for this zFiber node, delete to continue?', button=['Yes','No'])
            if confirm == "Yes":
                loaMuscleCustomDel(zFiber = zFiber)
            if confirm == 'No':
                return

    mm.eval("ziva -loa")
    renameNodes()

def loaDelete(): 
    # Deletes Ziva LOA setup
    muscleObjs = cmds.ls(sl=True)
    for muscle in muscleObjs:
        shapeNode = cmds.listRelatives(muscle, shapes=True)
        if not cmds.nodeType(shapeNode[0])== 'mesh':
            raise TypeError('selection is not a muscle of type mesh. Select muscle and run')
        else:
            if cmds.objExists(str(muscle)+"_zLineOfAction"):
                cmds.delete(str(muscle)+"_zLineOfAction")

def singleFiberSelectDelFiber():
    # Used when delSelFiber() finds multiple fibers
    def menuItem(item):
        cmds.layoutDialog( dismiss=item )

    # Get the dialog's formLayout.
    form = cmds.setParent(q=True)
    # layoutDialog's are not resizable, so hard code a size here,
    # to make sure all UI elements are visible.
    cmds.formLayout(form, e=True, width=400)
    
    fiberList = mm.eval('zQuery -t "zFiber" {0}'.format(cmds.ls(sl=True)[0]))

    t = cmds.text(l='Select zFiber node to delete LOA setup from')
    m1 = cmds.optionMenu(cc=menuItem)
    for item in fiberList:
        cmds.menuItem(item)

    spacer = 5
    top = 5
    edge = 5

    cmds.formLayout(form, edit=True,
                    attachForm=[(t, 'top', top), (t, 'left', edge), (t, 'right', edge), (m1, 'top', edge), (m1, 'left', edge), (m1, 'right', edge)],
                    attachNone=[(t, 'bottom'), (m1, 'bottom')],
                    attachControl=[(m1, 'top', spacer, t)],
                    attachPosition=[(m1, 'right', spacer, 100), (t, 'right', spacer, 100)])

def singleFiberSelectLOACustom():
    # Used when LOAMuscleCustom() finds multiple fibers
    def menuItem(item):
        cmds.layoutDialog( dismiss=item )

    # Get the dialog's formLayout.
    form = cmds.setParent(q=True)
    # layoutDialog's are not resizable, so hard code a size here,
    # to make sure all UI elements are visible.
    cmds.formLayout(form, e=True, width=400)
    
    fiberList = mm.eval('zQuery -t "zFiber" {0}'.format(cmds.ls(sl=True)[1]))

    t = cmds.text(l='Select zFiber node to use in custom LOA setup')
    m1 = cmds.optionMenu(cc=menuItem)
    for item in fiberList:
        cmds.menuItem(item)

    spacer = 5
    top = 5
    edge = 5

    cmds.formLayout(form, edit=True,
                    attachForm=[(t, 'top', top), (t, 'left', edge), (t, 'right', edge), (m1, 'top', edge), (m1, 'left', edge), (m1, 'right', edge)],
                    attachNone=[(t, 'bottom'), (m1, 'bottom')],
                    attachControl=[(m1, 'top', spacer, t)],
                    attachPosition=[(m1, 'right', spacer, 100), (t, 'right', spacer, 100)])

def loaMuscleCustom(flexionValue, extensionValue, growthScale, loaFlexionChk, loaExtensionChk, inputCrv=None, zFiber=None, selMuscle=None):
    # LOA Setup Custom / inputCrv & zFiber & selMuscle comes from mirrorMuscleLR
    print('curve', inputCrv, '*'*30)
    print('muscle', selMuscle, '<'*30)
    if not (inputCrv==None and zFiber==None and selMuscle==None): # provided if mirroring musclesLR
        cmds.select(inputCrv, r=True)
        cmds.select(selMuscle, add=True)
    

    # Make sure LOA curve and muscle are in correct selection order (LOA Curve, then Muscle)
    currentSelection = cmds.ls(sl=True)
    if len(currentSelection) != 2:
        raise TypeError('Select LOA curve then muscle before using this function. Err1')

    if cmds.objectType(omu.getDagPath(currentSelection[0], shape=True)) != 'nurbsCurve': # Make sure nurbsCurve
        raise TypeError('Select LOA curve first, then muscle before using this function. Err2')

    if cmds.objectType(omu.getDagPath(currentSelection[1], shape=True)) == 'mesh': # Make sure mesh is a zTissue
        tissueShape = omu.getDagPath(currentSelection[1], shape=True)
        if 'zEmbedder' not in cmds.listConnections(tissueShape+'.inMesh')[0]:
            raise TypeError('Select LOA curve first, then muscle before using this function')

    inputCrv = currentSelection[0]
    selMuscle = currentSelection[1]

    # Make sure the muscle has a zFiber
    if not mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle)): # Check for existing fiber:
        confirm = cmds.confirmDialog(message = 'No zFiber on this muscle. Create it?', button=['Yes','No'])
        if confirm == 'Yes':
            addFiber(muscle=selMuscle)
            zFiber = mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle)) # Have to re-query as addFiber() renames nodes and cannot return node unless I switch to pm instead of cmds.
        if confirm == 'No':
            return

    # Check for multiple zFibers on muscle. Prompt user to select fiber if multiple.
    if len(mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle))) >1:
        zFiber = cmds.layoutDialog(ui=singleFiberSelectLOACustom)
        if zFiber == 'dismiss':
            raise AttributeError('No zFiber seleted. Aborting')
    else:
        zFiber = mm.eval('zQuery -t "zFiber" {0}'.format(selMuscle))[0]

    # Make sure there is no Ziva LOA setup already.
    musLoa = cmds.listConnections(zFiber, type='zLineOfAction')
    if musLoa:
        confirm = cmds.confirmDialog(message = 'Ziva zLineOfAction already setup for this zFiber node. delete it to continue?', button=['Yes','No'])
        if confirm == 'Yes':
            cmds.delete(musLoa)
        if confirm == 'No':
            return

    # Make sure there is no custom LOA setup already.
    if cmds.listConnections(zFiber, type='remapValue'):
        if 'LOACustomMuscleGrowth' in cmds.listConnections(zFiber, type='remapValue')[0]:
            confirm = cmds.confirmDialog(message = 'Custom LOA already exists for this zFiber node, delete to continue?', button=['Yes','No'])
            if confirm == "Yes":
                loaMuscleCustomDel(zFiber = zFiber)
            if confirm == 'No':
                return






    # Create custom LOA nodes
    flex      = cmds.createNode('remapValue', name=str(selMuscle)+'_LOACustomFlexion', ss=True)
    exten     = cmds.createNode('remapValue', name=str(selMuscle)+'_LOACustomExtension', ss=True)
    combine   = cmds.createNode('plusMinusAverage', name=str(selMuscle)+'_LOACustomFlexExtCombine', ss=True)
    crvLen    = cmds.createNode('curveInfo', name=str(selMuscle)+'_LOACustomCurveInfo', ss=True)
    growthScl = cmds.createNode('remapValue', name=str(selMuscle)+'_LOACustomGrowth', ss=True)
    # Create attr for backup of settings
    cmds.addAttr(crvLen, ci=True, at='float', sn='flexionValue')
    cmds.addAttr(crvLen, ci=True, at='float', sn='extensionValue')
    cmds.addAttr(crvLen, ci=True, at='float', sn='growthScale')
    cmds.addAttr(crvLen, ci=True, at='bool', sn='loaFlexionChk')
    cmds.addAttr(crvLen, ci=True, at='bool', sn='loaExtensionChk')
    cmds.addAttr(crvLen, ci=True, dt='string', sn='musFiber')
    cmds.addAttr(crvLen, ci=True, dt='string', sn='inputCrv')
    if loaFlexionChk:
        cmds.setAttr(crvLen+'.flexionValue', flexionValue)
    else:
        cmds.setAttr(crvLen+'.flexionValue', 0)
    if loaExtensionChk:
        cmds.setAttr(crvLen+'.extensionValue', extensionValue)
    else:
        cmds.setAttr(crvLen+'.extensionValue', 0)
    # muscle growth
    cmds.setAttr(growthScl+'.outputMin', 1.0)
    cmds.setAttr(crvLen+'.loaExtensionChk', loaExtensionChk)
    cmds.setAttr(crvLen+'.loaFlexionChk', loaFlexionChk)
    cmds.setAttr(crvLen+'.growthScale', growthScale)
    cmds.setAttr(crvLen+'.musFiber', zFiber, type='string')
    cmds.setAttr(crvLen+'.inputCrv', inputCrv, type='string')
    # Connect custom LOA nodes
    cmds.connectAttr(inputCrv+'.worldSpace[0]', crvLen+'.inputCurve', f=1)
    cmds.connectAttr(crvLen+'.arcLength', flex+'.inputValue', f=1)
    cmds.connectAttr(crvLen+'.arcLength', exten+'.inputValue', f=1)
    cmds.connectAttr(flex+'.outValue', combine+'.input1D[0]', f=1)
    cmds.connectAttr(exten+'.outValue', combine+'.input1D[1]', f=1)
    cmds.connectAttr(combine+'.output1D', zFiber+'.excitation', f=1)
    cmds.connectAttr(combine+'.output1D', growthScl+'.inputValue', f=1)
    cmds.connectAttr(crvLen+'.growthScale', growthScl+'.outputMax', f=1)
    cmds.connectAttr(growthScl+'.outValue', zFiber+'.muscleGrowthScale', f=1)
    # Set values for custom LOA nodes
    if loaFlexionChk:
        cmds.setAttr(flex+'.inputMin', cmds.arclen(inputCrv))
        cmds.setAttr(flex+'.inputMax', flexionValue) # Current curve length
    else:
        cmds.setAttr(flex+'.inputMin', 0)
        cmds.setAttr(flex+'.inputMax', 0)

    if loaExtensionChk:
        cmds.setAttr(exten+'.inputMin', cmds.arclen(inputCrv)) # Current curve length
        cmds.setAttr(exten+'.inputMax', extensionValue)
    else:
        cmds.setAttr(exten+'.inputMin', 0)
        cmds.setAttr(exten+'.inputMax', 0)
    # Connect outValue to CurveInfo node so changes are stored
    cmds.connectAttr(flex+'.inputMax', crvLen+'.flexionValue', f=1)
    cmds.connectAttr(exten+'.inputMax', crvLen+'.extensionValue', f=1)
    # Disables fiber on curve flexion (when curve shortens in length)
    if loaFlexionChk == 0:
        cmds.setAttr(selMuscle+'_LOACustomFlexion.outputMax', 0.0)
    # Disables fiber on curve extension (when curve grows in length)
    if loaExtensionChk == 0:
        cmds.setAttr(selMuscle+'_LOACustomExtension.outputMax', 0.0)
    # Enable muscle growth scale
    if growthScale != 1.0:
        cmds.setAttr(zFiber+'.enableMuscleGrowth', 1)

def loaMuscleCustomDel(zFiber=None):
    # zFiber var comes from LOAMuscleCustom
    if zFiber:
        if cmds.objectType(zFiber) != 'zFiber':
            raise TypeError('Object passed to this function is not a zFiber node')
        else:
            fiberNodes = cmds.listHistory(cmds.listConnections(zFiber+'.muscleGrowthScale'), f=0, levels=4)
            for node in fiberNodes:
                if '_LOACustom' in node:
                    if cmds.objExists(node):
                        cmds.delete(node)
            cmds.setAttr(zFiber+'.excitation', 0)
            cmds.setAttr(zFiber+'.muscleGrowthScale', 0)
            return

    objList = cmds.ls(sl=True)
    for obj in objList:
        if cmds.objectType(obj) != 'zFiber':
            if cmds.objectType(omu.getDagPath(obj, shape=True)) == 'mesh':
                if mm.eval('zQuery -t "zFiber" {0}'.format(obj)):
                    # Check for multiple zFibers on muscle. Prompt user to select fiber if multiple.
                    if len(mm.eval('zQuery -t "zFiber" {0}'.format(obj))) >1:
                        cmds.select(obj)
                        zFiber = cmds.layoutDialog(ui=singleFiberSelectDelFiber)
                        if zFiber == 'dismiss':
                            continue
                        else:
                            fiberNodes = cmds.listHistory(cmds.listConnections(zFiber+'.muscleGrowthScale'), f=0, levels=4)
                            for node in fiberNodes:
                                if '_LOACustom' in node:
                                    if cmds.objExists(node):
                                        cmds.delete(node)
                    else:
                        zFiber = mm.eval('zQuery -t "zFiber" {0}'.format(obj))[0]
                        fiberNodes = cmds.listHistory(cmds.listConnections(zFiber+'.muscleGrowthScale'), f=0, levels=4)
                        for node in fiberNodes:
                            if '_LOACustom' in node:
                                if cmds.objExists(node):
                                    cmds.delete(node)
                        return
                else:
                    cmds.warning('Object is neither zFiber node, or Tissue mesh. Err1', obj)
                    continue
        if cmds.objectType(obj) == 'zFiber':
            fiberNodes = cmds.listHistory(cmds.listConnections(obj+'.muscleGrowthScale'), f=0, levels=4)
            for node in fiberNodes:
                if '_LOACustom' in node:
                    if cmds.objExists(node):
                        cmds.delete(node)
            cmds.setAttr(obj+'.excitation', 0)
            cmds.setAttr(obj+'.muscleGrowthScale', 0)

        else:
            cmds.warning('Object is neither zFiber node, or Tissue mesh. Err2', obj)
            continue

def loaCustomBackup():
    fileFilter = '*.json'
    savePath = cmds.fileDialog2(fm=0, okc="Save", ff=fileFilter)[0]
    if savePath:
        # Save LOA Curves
        customLOADict = loaCustomSettings()
        with open(savePath, 'w') as f: 
            json.dump({str(k):v for k, v in customLOADict.items()}, f) # Converts keys from tuples to strings

def loaCustomRestore():
    fileType = '*.json'
    loadPath = cmds.fileDialog2(fm = 1, okc = "Load", fileFilter = fileType)[0]
    # (i) load json object
    with open(loadPath, 'r') as f: obj = json.load(f, object_pairs_hook=OrderedDict)
    # (ii) convert loaded keys from string back to tuple
    customLOADict={literal_eval(k):v for k, v in obj.items()}

    # Create LOA Curves
    for key , value in customLOADict.items():
        if  key[0] == 'loaCurveBkup':
            loaCrv = cmds.curve(p=[list(value.items())[0][1][0], list(value.items())[1][1][0]], k=[0, 1], d=1)# Cluster0Pos / Cluster1Pos
            crvShape = omu.getDagPath(loaCrv, shape=True)
            cmds.setAttr(crvShape+'.overrideEnabled', 1)
            cmds.setAttr(crvShape+'.overrideColor', 14)
            loaClusters = clusterOnCurve(loaCrv)
            if cmds.objExists(list(value.items())[2][1][0]): # Cluster 0 Parent
                loaRivet([loaClusters[0], list(value.items())[2][1][0]])
            if cmds.objExists(list(value.items())[3][1][0]): # Cluster 1 Parent
                loaRivet([loaClusters[1], list(value.items())[3][1][0]])
            if cmds.objExists(list(value.items())[4][1][0]): # loaParent
                cmds.parent(loaCrv, list(value.items())[4][1][0])
            cmds.rename(loaCrv, key[1])

    # Connect LOA Curves
    buildLOA = []
    for key , value in customLOADict.items():
        if key[0] == 'loaNodeBkup':
            inputCrv = key[1] # curve
            zFiber = key[2] # fiber
            muscle = key[2][:-7]
            growthScale = list(value.items())[0][1] # growthScale
            loaFlexionChk = list(value.items())[1][1] # loaFlexionChk
            loaExtensionChk = list(value.items())[2][1] # loaExtensionChk
            flexionValue = list(value.items())[3][1] # flexionValue
            extensionValue = list(value.items())[4][1] # extensionValue
            buildLOA.append([flexionValue[0], extensionValue[0], growthScale[0], loaFlexionChk[0], loaExtensionChk[0], inputCrv, zFiber, muscle])
        else:
            pass

    if buildLOA:
        for build in buildLOA:
            loaMuscleCustom(*build) # '*' will unpack list

loaParentDict = {}
allLoaCurves = []

def loaUnParent(writeOut=False, loaCurves=None):
    # allLoaCurves variable comes from autoUpdateMuscle.replaceMuscle(), when updating muscle geo.
    # Using global variables so it is not necessary to write out parenting info to file.
    global loaParentDict
    global allLoaCurves
    loaParentDict={}
    allLoaCurves=[]

    if loaCurves:
        allLoaCurves = loaCurves
    else: # Get all LOA Curves in scene
        [allLoaCurves.append(loaCurve) for loaCurve in cmds.ls('*_LOA_Curve', tr=True) if cmds.objectType(omu.getDagPath(loaCurve, shape=True))=='nurbsCurve']

    # Add curve and curve parent to loaParentDict
    if allLoaCurves:
        # Get Loa Parent
        [loaParentDict.update({loaCurve : cmds.listRelatives(loaCurve, p=True)[0].lower()}) for loaCurve in allLoaCurves if cmds.listRelatives(loaCurve, p=True)]
        # Move to world if not already
        [cmds.parent(loaCurve, w=True) for loaCurve in allLoaCurves if cmds.listRelatives(loaCurve, p=True)]
        # Re-Cluster
        [clusterOnCurve(loaCurve) for loaCurve in allLoaCurves]

        # If Write Out selected. Save loaParentDict to file in Maya current project directory
        if writeOut == True:
            # get current project directory
            projectDirectory = cmds.workspace(q=True, rd=True)
            # look for /scripts/cmZiva. Create it if it does not exist
            if not os.path.exists(projectDirectory+'/scripts/cmZiva'):
                os.makedirs(projectDirectory+'/scripts/cmZiva')
            # save to file:
            with open(projectDirectory+'/Scripts/cmZiva/loaDict.json', 'w') as f:
                data = loaParentDict
                json.dump(data, f)

        cmds.select(cl=True)

def loaReParent(readIn=False):
    global loaParentDict
    global allLOACurves

    allMuscles = cmds.ls('Mus_*', type='transform')
    muscleListLowercase = [name.lower() for name in allMuscles]

    # check to see if reading parentDict from file
    if readIn == True:
        # get current project directory
        projectDirectory = cmds.workspace(q=True, rd=True)

        if os.path.isfile(projectDirectory+'/Scripts/cmZiva/loaDict.json'):
            loaParentDict = {}
            with open(projectDirectory+'/Scripts/cmZiva/loaDict.json') as json_file:  
                loaParentDict = json.load(json_file)
        else:
            cmds.warning('Parenting information has not been stored out')


    for loaCurve, loaParent in loaParentDict.items():
        if cmds.objExists(loaCurve):
            if loaParent in muscleListLowercase:
                cmds.parent(loaCurve, allMuscles[muscleListLowercase.index(loaParent)])
                autoRivetCrv(loaCurve)

def reconnectCustomLOA(muscleBuildList):
    #muscleBuildList comes from autoUpdateMuscle.py
    if muscleBuildList:
        muscleBuildList = muscleBuildList
    else:
        muscleBuildList = cmds.ls('Mus_*_Part*')

    for muscle in muscleBuildList:
        if cmds.objExists(str(muscle)+'_LOACustomCurveInfo'):
            cmds.connectAttr(str(muscle)+'_LOACustomFlexExtCombine.output1D', str(muscle)+'_zFiber.excitation', f=1)
            cmds.connectAttr(str(muscle)+'_LOACustomMuscleGrowth.outValue', str(muscle)+'_zFiber.muscleGrowthScale', f=1)
    cmds.warning('Done reconnecting Custom LOA Nodes')

def toggleLOACurve():
    loaVisible = []
    loaHidden = []

    curveList = pm.ls("*_LOA_Curve")
    for LOACurve in curveList:
        if LOACurve.visibility.get() == 0:
            loaHidden.append(LOACurve)
        if LOACurve.visibility.get() == 1:
            loaVisible.append(LOACurve)
            
    if len(loaVisible) > len(loaHidden):
        pm.hide(curveList)
    else:
        pm.showHidden(curveList)

def loaZivaSettings(zLineOfActionNodes=[]):
    '''
    Saves Ziva default LOA setup
    '''
    pass
    '''
    loaAttrList = []
    loaAttrDict = {}

    if zLineOfActionNodes != []:
        for node in zLineOfActionNodes:
            loaAttrs = cmds.listAttr(node, k=True)
            for attr in loaAttrs:
                loaAttrVal = cmds.getAttr(node+"."+attr)
                loaAttrList.append((attr,loaAttrVal))
                
            loaAttrDict[node] = loaAttrList

    return loaAttrDict
    '''

def loaCustomSettings(curves=True, curveInfoNodes=[], saveSel=False):
    '''
    Saves custom LOA setup.
    curves         = (bol) Save data to rebuild loa curve and cluster constraints
    curveInfoNodes = ([]) Supplied 'curveInfo' nodes from mirrorMuscleLR()
    saveSel        = (bol) Used when backing up ziva selection - zWindowQtUi.py > storeSelectedSetup()
    '''
    
    if saveSel == True:
        curveInfoNodes = []
        zivaSel = cmds.ls(sl=True)

        # Determine which items have a zFiber
        selFiber = []
        for sel in zivaSel:
            if mm.eval('zQuery -t "zFiber" {nde}'.format(nde=sel)):
                selFiber.append(mm.eval('zQuery -t "zFiber" {nde}'.format(nde=sel))[0])
        
        # Store curveInfo nodes
        if selFiber != []:
            for f in selFiber: # Determine if usig custom LOA curve
                if cmds.listConnections(f+'.muscleGrowthScale') != []:
                    node = cmds.listConnections(f+'.muscleGrowthScale')[0]
                    if cmds.nodeType(node) == 'curveInfo':
                        curveInfoNodes.append(node)
    
    
    else:
        if curveInfoNodes == []: # Not supplied in function call
            allFiber = mm.eval('zQuery -t "zFiber"')
            curveInfoNodes = []
            # Store curveInfo nodes
            if allFiber != []:
                for f in allFiber: 
                    if cmds.listConnections(f+'.muscleGrowthScale') != []:# This attr is always connected with custom LOA setup
                        remapNde   = cmds.listConnections(f+'.muscleGrowthScale')[0]
                        crvInfoNde = cmds.listConnections(remapNde+'.outputMax')[0]
                        if cmds.nodeType(crvInfoNde) == 'curveInfo':
                            curveInfoNodes.append(crvInfoNde)



    customLOADict = OrderedDict()
    if curveInfoNodes != []:
        for node in curveInfoNodes:
            customLOASettings = OrderedDict()
            customLOASettings[node+'.growthScale'] = [cmds.getAttr(node+'.growthScale'), cmds.getAttr(node+'.growthScale', type=True)]
            customLOASettings[node+'.loaFlexionChk'] = [cmds.getAttr(node+'.loaFlexionChk'), cmds.getAttr(node+'.loaFlexionChk', type=True)]
            customLOASettings[node+'.loaExtensionChk'] = [cmds.getAttr(node+'.loaExtensionChk'), cmds.getAttr(node+'.loaExtensionChk', type=True)]
            customLOASettings[node+'.flexionValue'] = [cmds.getAttr(node+'.flexionValue'), cmds.getAttr(node+'.flexionValue', type=True)]
            customLOASettings[node+'.extensionValue'] = [cmds.getAttr(node+'.extensionValue'), cmds.getAttr(node+'.extensionValue', type=True)]

            customLOADict['loaNodeBkup', cmds.getAttr(node+'.inputCrv'), cmds.getAttr(node+'.musFiber')] = customLOASettings # Add attrs dict to backup dict


    if curves == True: # Save the curve point positions and cluster constraints
        for node in curveInfoNodes:
            if cmds.listConnections(node+'.inputCurve') != None: # Make a sure curve is connected
                crv = cmds.listConnections(node+'.inputCurve')[0]
                
                customLOACurves = OrderedDict()
                crvShape = omu.getDagPath(crv, shape=True)
                crvPnt0 = cmds.getAttr(crvShape+'.editPoints[0]')[0]
                crvPnt1 = cmds.getAttr(crvShape+'.editPoints[1]')[0]

                customLOACurves['cluster0Pos']=[crvPnt0]
                customLOACurves['cluster1Pos']=[crvPnt1]
                customLOACurves['cluster0parent']=[cmds.getAttr(crv+'.cluster0parent'), cmds.getAttr(crv+'.cluster0parent', type=True)]
                customLOACurves['cluster1parent']=[cmds.getAttr(crv+'.cluster1parent'), cmds.getAttr(crv+'.cluster1parent', type=True)]
                customLOACurves['loaParent']=[cmds.listRelatives(crv, p=True)[0]]

                customLOADict['loaCurveBkup', crv] = customLOACurves # Add attrs dict to backup dict


    return customLOADict



######################################## zTools Tab ########################################

def blendBones(s, e, driverBoneSuffix):
    if cmds.ls(sl=True):
        driverList = cmds.ls(sl=True)
        for i in driverList:
            print(i[:-len(driverBoneSuffix)])
            if cmds.objExists(i[:-len(driverBoneSuffix)]):
                print(i, i[:-len(driverBoneSuffix)])
                blendshape = cmds.blendShape(i, i[:-len(driverBoneSuffix)], n="ziva_geo_blendshape", origin="world")
                cmds.setKeyframe( blendshape, attribute=i, t=s, v=0)
                cmds.setKeyframe( blendshape, attribute=i, t=e, v=1)

def delBlendBones(driverBoneSuffix):
    if cmds.ls(sl=True):
        selectedObj = cmds.ls(sl=True)
        shapesList = []
        for selection in selectedObj:
            findShapes = pm.listRelatives(pm.PyNode(selection), type="shape")
            if findShapes:
                for shape in findShapes:
                    if "Orig" not in str(shape):
                        historyList = pm.listHistory(shape, af=True)
                        for node in historyList:
                            if pm.nodeType(node) == "blendShape":
                                shapesList.append(node)
        if shapesList:
            for node in shapesList:
                if pm.objExists:
                    pm.delete(node)
        return


    else:
        blendShapeDict = {}
        shapesList = []
        # find all objects with driverBoneSuffix
        driverObjects = cmds.ls('*'+driverBoneSuffix)

        # get all polymesh objects that could possibly be used as a match to driverObjects
        allObjs = cmds.ls(tr=True)
        allPolyMeshes = cmds.filterExpand(allObjs, sm=12)
        allPolyLower = [poly.lower() for poly in allPolyMeshes]

        for item in driverObjects:
            # item, lowercase, minus suffix, found in allPolyLower? Add item and match index to dict.
            if item.lower()[:-len(driverBoneSuffix)] in allPolyLower:
                blendShapeDict.update({item : allPolyLower.index(item.lower()[:-len(driverBoneSuffix)])})

        if blendShapeDict != {}:
             for key , value in blendShapeDict.items():
                # use index from allPolyMeshes list
                findShapes = pm.listRelatives(pm.PyNode(allPolyMeshes[value]), type="shape")
                if findShapes:
                    for shape in findShapes:
                        if "Orig" not in str(shape):
                            historyList = pm.listHistory(shape, af=True)
                            for node in historyList:
                                if pm.nodeType(node) == "blendShape":
                                    shapesList.append(node)
        # delete the blendshapes
        if shapesList:
            for node in shapesList:
                if pm.objExists:
                    pm.delete(node)

def keyRunUp(itemList=[], offFrame=None, onFrame=None, dampPadding=None):
    if itemList != []:
        for item in itemList:
            #determine if tissue or cloth and get either zCloth or zTissue node
            if mm.eval('zQuery -t "zTissue" {0}'.format(item)):
                zNode = mm.eval('zQuery -t "zTissue" {0}'.format(item))[0]

            elif mm.eval('zQuery -t "zCloth" {0}'.format(item)):
                zNode = mm.eval('zQuery -t "zCloth" {0}'.format(item))[0]

            else:
                print (str(item), '<<<<<<<------------------------------------------------------')
                cmds.warning('Selection is not zTissue or zCloth')
                continue

            # Delete existing frames
            cmds.cutKey(zNode, attribute=['rSE', 'pE', 'sTE', 'iD'])

            ### OFF FRAME ###
            cmds.setKeyframe(zNode+'.rSE', t=offFrame, v=0) #restScaleEnvelope
            cmds.setKeyframe(zNode+'.pE', t=offFrame, v=0) #pressureEnvelope
            cmds.setKeyframe(zNode+'.sTE', t=offFrame, v=0) #surfaceTensionEnvelope
            cmds.setKeyframe(zNode+'.iD', t=offFrame, v=1) #inertialDamping
            ### ON FRAME ###
            cmds.setKeyframe(zNode+'.rSE', t=onFrame, v=1) #restScaleEnvelope
            cmds.setKeyframe(zNode+'.pE', t=onFrame, v=1) #pressureEnvelope
            cmds.setKeyframe(zNode+'.sTE', t=onFrame, v=1) #surfaceTensionEnvelope
            cmds.setKeyframe(zNode+'.iD', t=onFrame, v=0) #inertialDamping
            # Ease in tangents
            cmds.keyTangent(zNode+'_restScaleEnvelope', itt='spline', e=1)
            cmds.keyTangent(zNode+'_pressureEnvelope', itt='spline', e=1)
            cmds.keyTangent(zNode+'_surfaceTensionEnvelope', itt='spline', e=1)
            ### ID BUFFER ###
            cmds.setKeyframe(zNode+'.iD', t=(onFrame+dampPadding), v=0)
    else:
        return IndexError('No items provided to key')


def createWrap(driverBoneSuffix):
    wrapDict = {}

    if cmds.ls(sl=True) != None:
        allPolyMeshes = cmds.ls(sl=True)
        allPolyLower = [poly.lower() for poly in allPolyMeshes]

        # find all objects with driverBoneSuffix
        driverObjects = cmds.ls('*'+driverBoneSuffix)

        for item in driverObjects:
            # item, lowercase, minus suffix, found in allPolyLower? Add item and match index to dict.
            if item.lower()[:-len(driverBoneSuffix)] in allPolyLower:
                wrapDict.update({item : allPolyLower.index(item.lower()[:-len(driverBoneSuffix)])})
        # make the wraps
        if wrapDict != {}:
             for key , value in wrapDict.items():
                cmds.cvWrap(allPolyMeshes[value], key, name='ZivaCVWrap_'+str(key), radius=0.1)

    else:
        # find all objects with driverBoneSuffix
        driverObjects = cmds.ls('*'+driverBoneSuffix)

        # get all polymesh objects that could possibly be used as a match to driverObjects
        allObjs = cmds.ls(tr=True)
        allPolyMeshes = cmds.filterExpand(allObjs, sm=12)
        allPolyLower = [poly.lower() for poly in allPolyMeshes]

        for item in driverObjects:
            # item, lowercase, minus suffix, found in allPolyLower? Add item and match index to dict.
            if item.lower()[:-len(driverBoneSuffix)] in allPolyLower:
                wrapDict.update({item : allPolyLower.index(item.lower()[:-len(driverBoneSuffix)])})
        # make the wraps
        if wrapDict != {}:
             for key , value in wrapDict.items():
                cmds.cvWrap(allPolyMeshes[value], key, name='ZivaCVWrap_'+str(key), radius=0.1)

def deleteWrap():
    selectedObjs = cmds.ls(sl=True)

    for item in selectedObjs:
        wrapNodes = []
        wrapNodes = cmds.ls('ZivaCVWrap_' + item + '*')
        if wrapNodes != []:
            for node in wrapNodes:
                if cmds.objExists(node):
                    cmds.delete(node)

def multiObjImport(importFileList):
    if importFileList:
        for file in importFileList:
            if os.path.isfile(file[:-4]+'.mtl'):
                os.remove(file[:-4]+'.mtl')
                #importFileList.remove(file)

        for file in importFileList:
            importObj = cmds.file(file, i = True, returnNewNodes=True, type="OBJ")

            splitSlash = file.split("/")
            newName = cmds.rename(importObj[0], splitSlash[-1][:-4])

            zivaName = newName.split("_")
            if len(zivaName) > 3:
                if "v" in zivaName[0] and "Part" in zivaName[4]:
                    cmds.rename(newName, newName[5:])

def multiObjExport():
    fileType = 'Directories'
    savePath = cmds.fileDialog2(fm = 3, okc = "Select Directory", fileFilter = fileType)
    if savePath:
        curentObjectSelection = cmds.ls(sl=1,fl=1)
        for item in curentObjectSelection:
            if cmds.objectType(omu.getDagPath(item, shape=True))=='mesh':
                finalExportPath = "%s/%s.obj"%(savePath[0], item)
                cmds.select(item)
                cmds.file(finalExportPath, es=True, force=True, type='OBJexport', op='groups=0;ptgroups=0;materials=0;smoothing=1;normals=1', pr=True)
            else:
                print ("Ignoring object named: '%s'. Export failed, probably not a polygonal object. "%(item))
        print ("Exporting Complete!")
    else:
        print ("user cancelled")

def matchPointPosition(meshList=None):
    if meshList:
        meshUpdateList = meshList
    else:
        meshUpdateList = cmds.ls(sl=True)

    if len(meshUpdateList) == 2:
        # assigning variables to selected objects
        newTrans, deformedTrans = meshUpdateList
        # look for multiple origShapes on mesh that is to be updated.
        deformedShapes = cmds.listRelatives (deformedTrans , shapes=True, path=True)

        # if multiple Orig shapes are found
        if len(deformedShapes) > 2:
            cmds.warning(deformedTrans+" "+"Multiple Orig Shapes. Remove unused deformers until 'Removed 0 Deformers'")
            mm.eval("cleanUpScene 2")
            return

        if len(deformedShapes) > 1:
            # gets Orig shape node
            deformedShapeOrig = [el for el in deformedShapes if "Shape" and "Orig" in el] [0]
            res,= cmds.duplicate(deformedShapeOrig , name = "UpdateShapeDuplicate")
            theShapesDup = cmds.listRelatives (res, s=True, path=True) [0]
            cmds.connectAttr(theShapesDup +".outMesh", deformedShapeOrig +".inMesh", f=True)
            createdBS,=cmds.blendShape(meshUpdateList[0], res,  w=[0,1])
            cmds.refresh() # wait a tick
            cmds.delete(res, newTrans)
        else:
            cmds.connectAttr(newTrans +".outMesh", deformedTrans +".inMesh", f=True)
            cmds.refresh() # wait a tick
            cmds.delete(newTrans)
    else:
        cmds.warning("Select two objects. Object to update last")

def cometRename():
    mm.eval("cometRename;")
    cometRename;

def formatName():
    selectedObj = cmds.ls(sl=True)
    for item in selectedObj:
        cmds.rename(item, item.lower())
        cmds.rename(item.lower(), item.title())

######################################## Utils ########################################

def convertRLName(name):
    """Convert a string with underscore

    i.e: "_\L", "_L0\_", "L\_", "_L" to "R". And vice and versa.

    :param string name: string to convert
    :return: Tuple of Integer

    """
    if name == "L":
        return "R"
    elif name == "R":
        return "L"
    re_str = "_[RL][0-9]+_|^[RL][0-9]+_|_[RL][0-9]+$|_[RL]_|^[RL]_|_[RL]$"
    rePattern = re.compile(re_str)

    reMatch = re.search(rePattern, name)
    if reMatch:
        instance = reMatch.group(0)
        if instance.find("R") != -1:
            rep = instance.replace("R", "L")
        else:
            rep = instance.replace("L", "R")

        name = re.sub(rePattern, rep, name)

    return name