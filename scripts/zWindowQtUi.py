from PySide2 import QtCore, QtGui, QtWidgets
import maya.cmds as cmds
import maya.mel as mm
import maya.OpenMayaUI as omui
import shiboken2 # literal_eval
import json
import os.path
from shiboken2 import wrapInstance
from collections import OrderedDict
from ast import literal_eval
import zBuilder.builders.ziva as zva
import omUtil as omu

import zivaTools as zTool
import importlib
importlib.reload(zTool)

import zWindow as customUI

import autoUpdateMuscle as aum
importlib.reload(aum)

import transform as tra

import zBuilder.utils as uz

#############################################################################################

def maya_main_window():
    main_window_ptr = omui.MQtUtil.mainWindow()
    return shiboken2.wrapInstance(int(main_window_ptr), QtWidgets.QWidget)
     
         
class ControlMainWindow(QtWidgets.QMainWindow):

    
    def __init__(self, parent=None):

        super(ControlMainWindow, self).__init__(parent)
        self.setWindowFlags(QtCore.Qt.Tool)
        self.ui = customUI.Ui_zivaWindow()
        self.ui.setupUi(self)

        ############################## zSetup Tab ##############################
        # zSetup
        self.ui.addSolver_Btn.clicked.connect(self.addSolver)
        self.ui.addBone_Btn.clicked.connect(self.addBone)
        self.ui.addTissue_Btn.clicked.connect(self.addTissues)
        self.ui.addFiber_Btn.clicked.connect(zTool.addFiber)
        self.ui.addAttachment_Btn.clicked.connect(self.addAttachment)
        self.ui.addCloth_Btn.clicked.connect(self.addCloth)
        self.ui.addzCache_Btn.clicked.connect(self.addzCache)

        self.ui.selectProximity_Btn.clicked.connect(self.selectByProximity)

        # Tools
        self.ui.meshCheck_Btn.clicked.connect(self.meshCheck)
        self.ui.findIntersections_Btn.clicked.connect(self.findIntersections)
        self.ui.computeVol_Btn.clicked.connect(self.computeVolume)
        self.ui.renameNodes_Btn.clicked.connect(zTool.renameNodes)
        self.ui.toggleMuscle_Btn.clicked.connect(self.toggleMuscle)
        self.ui.mirrorMuscleLR_Btn.clicked.connect(zTool.mirrorMuscleLR)
        self.ui.zBuilderWindow_Btn.clicked.connect(zTool.zBuilderWindow)

        # Select Multi
        self.ui.selectAll_Btn.clicked.connect(self.selectAllDecide)

        # New muscle default settings
        self.ui.usrTetSize_Btn.clicked.connect(self.setTet)
        self.ui.usrContactStiff_Btn.clicked.connect(self.setContactStiff)
        self.ui.usrCompRes_Btn.clicked.connect(self.setCompRes)
        self.ui.usrPoisRatio_Btn.clicked.connect(self.setPoisRatio)
        self.ui.usrVolumeCons_Btn.clicked.connect(self.setVolumeCons)

        # Remove
        self.ui.delAttachment_Btn.clicked.connect(self.delSelAttach)
        self.ui.delFiber_Btn.clicked.connect(zTool.delSelFiber)
        self.ui.delzCache_Btn.clicked.connect(self.delzCache)
        self.ui.clearSelected_Btn.clicked.connect(self.clearSelected)
        self.ui.clearScene_Btn.clicked.connect(self.clearScene)
        self.ui.clearCache_Btn.clicked.connect(self.clearCache)

        # Cache
        self.ui.saveCache_Btn.clicked.connect(self.writeZivaCache)
        self.ui.loadCache_Btn.clicked.connect(self.readZivaCache)


        ############################## LOA Tab ##############################
        # Create LOA
        self.ui.createLOA_Btn.clicked.connect(zTool.createLOA)
        self.ui.LOARivet_Btn.clicked.connect(zTool.loaRivet)
        self.ui.clusterOnCurve_Btn.clicked.connect(self.clusterOnCurve)
        self.ui.clusterOnCV_Btn.clicked.connect(zTool.clusterOnCV)
        self.ui.LOAAutoRivet_Btn.clicked.connect(zTool.autoRivetCrv)

        # LOA Setup Ziva
        self.ui.LOAMuscle_Btn.clicked.connect(zTool.loaMuscle)
        self.ui.LOADelete_Btn.clicked.connect(zTool.loaDelete)

        # LOA Setup Custom
        self.ui.LOACurrentLengthFlexion_Btn.clicked.connect(self.loaCurrentLengthFlexion)
        self.ui.LOACurrentLengthExtension_Btn.clicked.connect(self.loaCurrentLengthExtension)
        self.ui.LOAMuscleCustom_Btn.clicked.connect(self.loaMuscleCustom)
        self.ui.LOAMuscleCustomDel_Btn.clicked.connect(zTool.loaMuscleCustomDel)
        self.ui.LOAMuscleCustomReconnect_Btn.clicked.connect(self.reconnectCustomLOA)

        self.ui.LOABackup_Btn.clicked.connect(zTool.loaCustomBackup)
        self.ui.LOARestore_Btn.clicked.connect(zTool.loaCustomRestore)
        self.ui.LOAUnParent_Btn.clicked.connect(self.loaUnParent)
        self.ui.LOAReParent_Btn.clicked.connect(self.loaReParent)
        self.ui.toggleLOACurve_Btn.clicked.connect(zTool.toggleLOACurve)


        ############################## zTools Tab ##############################
        # Update Muscle Gometry
        projectDir = cmds.workspace(q=True, rd=True)
        self.ui.updateGeoPath_Txt.setText(projectDir.replace("/", "\\"))
        self.ui.updateGeoPath_Txt.setCursorPosition( 0 )

        self.ui.browseMuscleGeoDir_Btn.clicked.connect(self.muscleDirBrowse)
        self.ui.updateMuscleGeo_Btn.clicked.connect(self.updateMuscleGeo)

        # zSave
        self.ui.storeSelSetup_Btn.clicked.connect(self.storeSelectedSetup)
        self.ui.storeSceneSetup_Btn.clicked.connect(self.storeSceneSetup)
        self.ui.retrieveSetup_Btn.clicked.connect(self.retrieveSetup)

        # Bone Blendshape
        self.ui.boneBlendshape_Btn.clicked.connect(self.blendBones)
        self.ui.boneBlendDel_Btn.clicked.connect(self.delBlendBones)
        self.ui.createWrap_Btn.clicked.connect(self.createWrap)
        self.ui.deleteWrap_Btn.clicked.connect(zTool.deleteWrap)

        self.ui.addSuffix_Btn.clicked.connect(self.addSuffix)
        self.ui.removeSuffix_Btn.clicked.connect(self.removeSuffix)

        # Muli Obj Import / Export
        self.ui.multiObjExp_Btn.clicked.connect(zTool.multiObjExport)
        self.ui.multiObjImp_Btn.clicked.connect(self.multiObjImport)

        # Miscellaneous
        self.ui.matchPointPos_Btn.clicked.connect(zTool.matchPointPosition)
        self.ui.cometRename_Btn.clicked.connect(zTool.cometRename)
        self.ui.capitalize_Btn.clicked.connect(zTool.formatName)
        self.ui.keyRunUp_Btn.clicked.connect(self.keyRunUp)


    ######################################## zSetup Tab ########################################
    def addSolver(self):
        mm.eval("ziva -s")

    def addBone(self):
        if cmds.ls(sl=True):
            boneList = []
            boneBuildList = []
            [boneList.append(i) for i in cmds.ls(sl=True) if cmds.objectType(omu.getDagPath(i, shape=True))=='mesh'] # Verify its a mesh
            if boneList:
                for bone in boneList:
                    boneSplit = None
                    boneSplit = bone.split("_")

                    if len(boneSplit) != 4:
                        print (str(bone), '<<<<<<<------------------------------------------------------')
                        raise IndexError('Bone not named properly. Example: Bne_L_BoneName_Part1. err1')
                    else: # Capitalize each word
                        boneFormat = cmds.rename(bone, bone.title())
                        boneSplit = boneFormat.split("_")

                    if str(boneSplit[0]) != 'Bne':
                        print (str(bone), '<<<<<<<------------------------------------------------------')
                        raise IndexError('Bone not named properly. Example: Bne_L_BoneName_Part1. err2')
                    if boneSplit[1] not in ['L', 'R', 'M', 'X']:
                        print (str(bone), '<<<<<<<------------------------------------------------------')
                        raise IndexError('Bone not named properly. Example: Bne_L_BoneName_Part1. err3')
                    else:
                        boneBuildList.append(boneFormat)

                if boneBuildList: # Mesh needs to be selected for Ziva
                    for bone in boneBuildList:
                        cmds.select(bone)
                        mm.eval("ziva -b")
                        zTool.renameNodes()
            else:
                raise AttributeError('No object of type "mesh" selected for bone')

    def addTissues(self):
        if cmds.ls(sl=True):
            tissueList = []
            tissueBuildList = []
            [tissueList.append(i) for i in cmds.ls(sl=True) if cmds.objectType(omu.getDagPath(i, shape=True))=='mesh'] # Verify its a mesh
            if tissueList:
                for tissue in tissueList:
                    # Check if already a tissue
                    if cmds.nodeType(cmds.listConnections(tissue+'.wm')) == 'zGeo':
                        pass
                    else:
                        tissueSplit = None
                        tissueSplit = tissue.split("_")

                        if len(tissueSplit) != 4:
                            print (str(tissue), '<<<<<<<------------------------------------------------------')
                            raise IndexError('Tissue not named properly. Example: Mus_L_TissueName_Part1. err1')
                        # else: # Capitalize each word
                        #     tissueFormat = cmds.rename(tissue, tissue.title())
                        #     tissueSplit = tissueFormat.split("_")
                        if str(tissueSplit[0]) != 'Mus':
                            print (str(tissue), '<<<<<<<------------------------------------------------------')
                            raise IndexError('Tissue not named properly. Example: Mus_L_TissueName_Part1. err2')
                        if tissueSplit[1] not in ['L', 'R', 'M']:
                            print (str(tissue), '<<<<<<<------------------------------------------------------')
                            raise IndexError('Tissue not named properly. Example: Mus_L_TissueName_Part1. err3')
                        else:
                            tissueBuildList.append(tissue)

                if tissueBuildList:
                    if self.ui.tetSize_Chk.isChecked():
                        tetChk = 1
                        usrTetSize = self.ui.zTet_Spn.value()
                    else:
                        tetChk = 0
                        usrTetSize = 0
                    if self.ui.contactStiffness_Chk.isChecked():
                        contStiffChk = 1
                        usrContStiff = self.ui.contactStiffness_Spn.value()
                    else:
                        contStiffChk = 0
                        usrContStiff = 0
                    if self.ui.compressionResistance_Chk.isChecked():
                        compResChk = 1
                        usrCompRes = self.ui.compressionResistance_Spn.value()
                    else:
                        compResChk = 0
                        usrCompRes = 0
                    if self.ui.poissonsRatio_Chk.isChecked():
                        poisRatioChk = 1
                        usrPoisRatio = self.ui.poissonsRatio_Spn.value()
                    else:
                        poisRatioChk = 0
                        usrPoisRatio = 0
                    if self.ui.volumeCons_Chk.isChecked():
                        volumeConsChk = 1
                        usrVolumeConsVal = self.ui.volumeCons_Spn.value()
                    else:
                        volumeConsChk = 0
                        usrVolumeConsVal = 0

                    for tissue in tissueBuildList:
                        cmds.select(tissue)
                        mm.eval("ziva -t")
                        zTool.renameNodes()
                        attrList = cmds.listHistory(tissue, allConnections=True, af=True)
                        if tetChk == 1:
                            zTetIndex = attrList.index(tissue+"_zTet")
                            cmds.setAttr(attrList[zTetIndex]+'.tetSize', usrTetSize)

                        if contStiffChk == 1:
                            zTissueIndex = attrList.index(tissue+"_zTissue")
                            cmds.setAttr(attrList[zTissueIndex]+'.contactStiffness', usrContStiff)

                        if compResChk == 1:
                            zTissueIndex = attrList.index(tissue+"_zTissue")
                            cmds.setAttr(attrList[zTissueIndex]+'.compressionResistance', usrCompRes)

                        if poisRatioChk == 1:
                            zMaterialIndex = attrList.index(tissue+"_zMaterial1")
                            cmds.setAttr(attrList[zMaterialIndex]+'.poissonsRatio', usrPoisRatio)
                        
                        if volumeConsChk == 1:
                            zMaterialIndex = attrList.index(tissue+"_zMaterial1")
                            cmds.setAttr(attrList[zMaterialIndex]+'.volumeConservation', usrVolumeConsVal)
            else:
                raise AttributeError('No object of type "mesh" selected for tissue')

    def addAttachment(self):
        if len(cmds.ls(sl=True)) >= 2:
            if len(cmds.ls(sl=True))==2: # Most likely obj selections
                firstObj, lastObj = cmds.ls(sl=True)[0], cmds.ls(sl=True)[1]

            elif len(cmds.ls(sl=True)) > 2: # Most likely vertex selection
                if 'vtx' in cmds.ls(sl=True)[0]:
                    firstObj, lastObj = cmds.ls(sl=True)[0].split('.')[0], cmds.ls(sl=True)[-1]


            spl = firstObj.split('_')
            exc = [i for i in spl if i not in['Mus', '', 'L', 'R', 'M']]
            for i in range(len(exc)):
                if exc[i].endswith('Shape'):
                    exc[i] = exc[i][:-5]
            firstObj = exc[0]+'_'+exc[1]


            spl = lastObj.split('_')
            exc = [i for i in spl if i not in['Mus', '', 'L', 'R', 'M']]
            for i in range(len(exc)):
                if exc[i].endswith('Shape'):
                    exc[i] = exc[i][:-5]
            lastObj = exc[0]+'_'+exc[1]


            attachNewName = firstObj+"______"+lastObj+'___'
            mm.eval("ziva -a")
            # Rename attachment
            findAttach = mm.eval('zQuery -t "zAttachment" {0}'.format(cmds.ls(sl=True)[0]))
            cmds.rename(findAttach[-1], attachNewName)
        else:
            raise IndexError('Select TWO objects to make an attachment')

    def addCloth(self):
        if cmds.ls(sl=True):
            clothList = []
            clothBuildList = []
            [clothList.append(i) for i in cmds.ls(sl=True) if cmds.objectType(omu.getDagPath(i, shape=True))=='mesh'] # Verify its a mesh
            if clothList:
                for cloth in clothList:
                    clothSplit = None
                    clothSplit = cloth.split("_")

                    if len(clothSplit) != 4:
                        print (str(cloth), '<<<<<<<------------------------------------------------------')
                        raise IndexError('Cloth not named properly. Example: Clo_L_ClothName_Part1. err1')
                    else: #Capitalize each word
                        clothFormat = cmds.rename(cloth, cloth.title())
                        clothSplit = clothFormat.split("_")

                    if clothSplit[0] != "Clo":
                        print (str(cloth), '<<<<<<<------------------------------------------------------')
                        raise IndexError('Cloth not named properly. Example: Clo_L_ClothName_Part1. err2')
                    if clothSplit[1] not in ['L', 'R', 'M']:
                        print (str(cloth), '<<<<<<<------------------------------------------------------')
                        raise IndexError('Cloth not named properly. Example: Clo_L_ClothName_Part1. err3')
                    else:
                        clothBuildList.append(clothFormat)

                if clothBuildList:
                    for cloth in clothBuildList:
                        cmds.select(cloth)
                        mm.eval("ziva -c")
                        zTool.renameNodes()
            else:
                raise AttributeError('No object of type "mesh" selected for cloth')

    def addzCache(self):
        mm.eval("ziva -acn")

    def selectByProximity(self, source=None, target=None, radius=0.1):
        if source and target:
            if cmds.objExists(source) and cmds.objExists(target):
                cmds.select(source, target)
                vertList = mm.eval('zFindVerticesByProximity -r ' + radius)
                cmds.select(None)
                if vertList:
                    for vert in vertList:
                        cmds.select(vert, add=True)
                cmds.select(dest, add=True)
        else:
            if cmds.ls(sl=1):
                if len(cmds.ls(sl=True)) != 2:
                    raise IndexError('Select only TWO objects')
                dest=cmds.ls(sl=True)[1]
                radius = str(self.ui.selectProximity_Spn.value())
                vertList = mm.eval('zFindVerticesByProximity -r ' + radius)
                cmds.select(None)
                if vertList:
                    for vert in vertList:
                        cmds.select(vert, add=True)
                cmds.select(dest, add=True)

    def meshCheck(self):
        mm.eval("zMeshCheck")

    def findIntersections(self):
        mm.eval("zFindIntersections")

    def computeVolume(self):
        mm.eval("computePolysetVolume")

    def toggleMuscle(self):
        if cmds.ls(sl=True):
            muscleList = []
            for obj in cmds.ls(sl=True):
                if mm.eval('zQuery -t "zTissue" {0}'.format(obj)): # Verify its a zTissue
                    muscleList.append(mm.eval('zQuery -t "zTissue" {0}'.format(obj))[0])
            if muscleList:
                for muscle in muscleList:
                    if cmds.getAttr(muscle+'.enable') == 1:
                        cmds.setAttr(muscle+'.enable', 0)
                    else:
                        cmds.setAttr(muscle+'.enable', 1)

    def selectAllDecide(self):
        selectIndex = self.ui.selectAll_Combo.currentIndex()
        selType = ['zBone', 'zTissue', 'zMaterial', 'zFiber', 'zAttachment']
        if cmds.ls(sl=True):
            selectedObjs = cmds.ls(sl=True)
            selList = []
            for obj in selectedObjs:
                findType = mm.eval('zQuery -t "{0}" {1}'.format(selType[selectIndex], obj))
                if findType:
                    selList.append(findType)
            if selList:
                cmds.select(None)
                for findType in selList:
                    cmds.select(findType, add=True)
        else:
            selList = []
            selList = cmds.ls(type = selType[selectIndex])
            if selList:
                cmds.select(None)
                for findType in selList:
                    cmds.select(findType, add=True)

    def setTet(self):
        if cmds.ls(sl=True):
            tetList = []
            tetSize = self.ui.zTet_Spn.value()
            for obj in cmds.ls(sl=True):
                if mm.eval('zQuery -t "zTet" {0}'.format(obj)): # Verify its a zTet
                    tetList.append(mm.eval('zQuery -t "zTet" {0}'.format(obj))[0])
            if tetList:
                for tet in tetList:
                    cmds.setAttr(tet+'.tetSize', tetSize)

    def setContactStiff(self):
        if cmds.ls(sl=True):
            tissueList = []
            stiff = self.ui.contactStiffness_Spn.value()
            for obj in cmds.ls(sl=True):
                if mm.eval('zQuery -t "zTissue" {0}'.format(obj)): # Verify its a zTissue
                    tissueList.append(mm.eval('zQuery -t "zTissue" {0}'.format(obj))[0])
            if tissueList:
                for tissue in tissueList:
                    cmds.setAttr(tissue+'.contactStiffness', stiff)

    def setCompRes(self):
        if cmds.ls(sl=True):
            tissueList = []
            compRes = self.ui.compressionResistance_Spn.value()
            for obj in cmds.ls(sl=True):
                if mm.eval('zQuery -t "zTissue" {0}'.format(obj)): # Verify its a zTissue
                    tissueList.append(mm.eval('zQuery -t "zTissue" {0}'.format(obj))[0])
            if tissueList:
                for tissue in tissueList:
                    cmds.setAttr(tissue+'.compressionResistance', compRes)

    def setPoisRatio(self):
        if cmds.ls(sl=True):
            matList = []
            poisRatio = self.ui.poissonsRatio_Spn.value()
            for obj in cmds.ls(sl=True):
                if mm.eval('zQuery -t "zMaterial" {0}'.format(obj)): # Verify its a zTissue
                    matList.append(mm.eval('zQuery -t "zMaterial" {0}'.format(obj))[0])
            if matList:
                for mat in matList:
                    cmds.setAttr(mat+'.poissonsRatio', poisRatio)

    def setVolumeCons(self):
        if cmds.ls(sl=True):
            matList = []
            consValue = self.ui.volumeCons_Spn.value()
            for obj in cmds.ls(sl=True):
                if mm.eval('zQuery -t "zMaterial" {0}'.format(obj)): # Verify its a zTissue
                    matList.append(mm.eval('zQuery -t "zMaterial" {0}'.format(obj))[0])
            if matList:
                for mat in matList:
                    cmds.setAttr(mat+'.volumeConservation', consValue)

    def delSelAttach(self):
        if cmds.ls(sl=True):
            attachList = []
            for obj in cmds.ls(sl=True):
                if mm.eval('zQuery -t "zAttachment" {0}'.format(obj)):
                    attachList.append(mm.eval('zQuery -t "zAttachment" {0}'.format(obj))) # List of attachments
            if attachList:
                for attach in attachList: # Attachment list
                    [cmds.delete(a) for a in attach if cmds.objExists(a)]

    def delzCache(self):
        cmds.delete("zSolver*Cache")

    def clearSelected(self):
        if cmds.ls(sl=True):
            for obj in cmds.ls(sl=True):
                mm.eval("ziva -rm")

    def clearScene(self):
        uz.clean_scene()

    def clearCache(self):
        mm.eval("zCache -c")

    def writeZivaCache(self):
        '''
        example: writeZivaCache(-5, 10, 'D:/test')
        '''
        inFrame = self.ui.saveCacheStart_Spn.value()
        outFrame = self.ui.saveCacheEnd_Spn.value()+1
        fileFilter = '*.zCache'
        savePath = cmds.fileDialog2(fm=0, okc="Save", startingDirectory=cmds.workspace(q=True, rd=True)+"\\cache\\ziva\\zivaCache", ff=fileFilter)[0]
        if savePath:
            savePath = savePath[:-7]+'___.%04i.zCache'
            cmds.progressWindow(isInterruptable=True)    

            for i in range(inFrame, outFrame):
                cmds.currentTime( i )
                if cmds.progressWindow(q=True, isCanceled=True):
                    break

                path = savePath % i
                mm.eval('zCache -save "%s";' % path)
                mm.eval('zCache -clear')

            cmds.progressWindow(endProgress = True)

    def readZivaCache(self):
        inFrame = self.ui.loadCacheStart_Spn.value()
        outFrame = self.ui.loadCacheEnd_Spn.value()+1
        fileFilter = '*.zCache'
        loadPath = cmds.fileDialog2(fm=1, okc="Load", ff=fileFilter)[0]

        if loadPath:
            # Get file name
            fileName = (loadPath.split('/')[-1]).split('___')[0]
            # Get file directory
            pathSplit = loadPath.split('/')
            pathJoin = '/'.join(pathSplit[:-1])+'/'

            for i in range(inFrame, outFrame):
                path = pathJoin+fileName+'___.%04i.zCache' % i
                print ('frame ', i)
                mm.eval('zCache -load "%s";' % path)

    ######################################## LOA Tab ########################################
    def clusterOnCurve(self):
        for item in cmds.ls(sl=True):
            if cmds.objectType(omu.getDagPath(item, shape=True)) == 'nurbsCurve':
                zTool.clusterOnCurve(item)

    def loaCurrentLengthFlexion(self):
        if cmds.ls(sl=True):
            currentSel = cmds.ls(sl=True)[0]
            if cmds.objectType(omu.getDagPath(currentSel, shape=True)) == 'nurbsCurve':
                if "LOA_Curve" in currentSel:
                    self.ui.curveFlexionLength_Spn.setValue(cmds.arclen(cmds.ls(sl=True)[0]))
                else:
                    raise IndexError('Select object is not of type (LOA) nurbsCurve, err2')
            else:
                raise IndexError('Select object is not of type (LOA) nurbsCurve, err1')

    def loaCurrentLengthExtension(self):
        if cmds.ls(sl=True):
            currentSel = cmds.ls(sl=True)[0]
            if cmds.objectType(omu.getDagPath(currentSel, shape=True)) == 'nurbsCurve':
                if "LOA_Curve" in currentSel:
                    self.ui.curveExtensionLength_Spn.setValue(cmds.arclen(cmds.ls(sl=True)[0]))
                else:
                    raise IndexError('Select object is not of type (LOA) nurbsCurve, err2')
            else:
                raise IndexError('Select object is not of type (LOA) nurbsCurve, err1')

    def loaMuscleCustom(self):
        flexionValue = self.ui.curveFlexionLength_Spn.value()
        extensionValue = self.ui.curveExtensionLength_Spn.value()
        growthScale = self.ui.muscleGrowth_Spn.value()
        loaFlexionChk = self.ui.loaFlexion_Chk.isChecked()
        loaExtensionChk = self.ui.loaExtension_Chk.isChecked()

        if loaFlexionChk or loaExtensionChk == True:
            zTool.loaMuscleCustom(flexionValue, extensionValue, growthScale, loaFlexionChk, loaExtensionChk)
        else:
            raise AttributeError('Check at least one LOA direction (Flexion or Extension)')

    def loaUnParent(self):
        writeOut = self.ui.writeOut_Chk.isChecked()
        zTool.loaUnParent(writeOut)

    def loaReParent(self):
        readIn = self.ui.readIn_Chk.isChecked()
        zTool.loaReParent(readIn)

    ######################################## zTools Tab ########################################
    def muscleDirBrowse(self):
        fileType = 'Directories'
        savePath = cmds.fileDialog2(fm=3, okc="Select Directory", startingDirectory=cmds.workspace(q=True, rd=True), fileFilter=fileType)[0]
        if savePath:
            formatPath = str(savePath)
            slashPath = formatPath.replace('/' , '\\')
            self.ui.updateGeoPath_Txt.setText(slashPath)
        else:
            print ("user cancelled")

    def updateMuscleGeo(self):
        if not cmds.ls(sl=1):
            raise IndexError('No zBones or zTissue selected for update')
        muscleDirPath = self.ui.updateGeoPath_Txt.text().replace("\\", "/")
        # values for keyRunUp
        offFrame = self.ui.keyOffFrame_Spn.value()
        onFrame = self.ui.keyOnFrame_Spn.value()
        dampPadding = self.ui.keyDamp_Spn.value()

        aum.replaceMuscle(muscleDirPath, offFrame, onFrame, dampPadding)

    def reconnectCustomLOA(self):
        zTool.reconnectCustomLOA(muscleBuildList=[])

    def storeSceneSetup(self):
        fileFilter = '*.ziva'
        savePath = cmds.fileDialog2(fm=0, okc="Save", ff=fileFilter)[0]
        if savePath:
            # Save LOA Curves
            customLOADict = zTool.loaCustomSettings()
            if customLOADict:
                with open(savePath[:-5]+'_Curves.json', 'w') as f: 
                    json.dump({str(k):v for k, v in customLOADict.items()}, f) # Converts keys from tuples to strings
            # Save Ziva
            z = zva.Ziva()
            z.retrieve_from_scene()
            z.write(savePath)

    def retrieveSetup(self):
        customLOADict={}
        fileType = '*.ziva'
        loadPath = cmds.fileDialog2(fm=1, okc="Load", fileFilter=fileType)[0]
        if loadPath:
            if os.path.exists(loadPath[:-5]+'_Curves.json'): # Check for LOA Curves json file
                # load json object
                with open(loadPath[:-5]+'_Curves.json', 'r') as f: obj = json.load(f, object_pairs_hook=OrderedDict)
                # convert loaded keys from string back to tuple
                customLOADict={literal_eval(k):v for k, v in obj.items()}

                # Create LOA Curves
                for key,value in customLOADict.items():
                    if not cmds.objExists(key[1]):# existing loa curve
                        if key[0] == 'loaCurveBkup':
                            p0=(list(value.values())[0][0]) #crv point0
                            p1=(list(value.values())[1][0]) #crv point1
                            c0=(list(value.values())[2][0]) #cluster0 parent
                            c1=(list(value.values())[3][0]) #cluster1 parent
                            l0=(list(value.values())[4][0]) #loa parent

                            loaCrv = cmds.curve(p=[p0, p1], k=[0, 1], d=1)
                            crvShape = omu.getDagPath(loaCrv, shape=True)
                            cmds.setAttr(crvShape+'.overrideEnabled', 1)
                            cmds.setAttr(crvShape+'.overrideColor', 14)
                            loaClusters = zTool.clusterOnCurve(loaCrv)
                            
                            if cmds.objExists(c0): # Cluster 0 Parent
                                zTool.loaRivet([loaClusters[0], c0])
                            if cmds.objExists(c1): # Cluster 1 Parent
                                zTool.loaRivet([loaClusters[1], c1])
                            if cmds.objExists(l0): # loaParent
                                cmds.parent(loaCrv, l0)
                            
                            cmds.rename(loaCrv, key[1])

            z = zva.Ziva()
            z.retrieve_from_file(loadPath)
            z.build()

            if customLOADict != {}:
                # Connect LOA Curves
                buildLOA = []
                for key,value in customLOADict.items():
                    if key[0] == 'loaNodeBkup':
                        inputCrv = key[1] # curve
                        zFiber   = key[2] # fiber
                        muscle   = key[2][:-8]
                        growthScale     = list(value.items())[0][1] # growthScale
                        loaFlexionChk   = list(value.items())[1][1] # loaFlexionChk
                        loaExtensionChk = list(value.items())[2][1] # loaExtensionChk
                        flexionValue    = list(value.items())[3][1] # flexionValue
                        extensionValue  = list(value.items())[4][1] # extensionValue
                        buildLOA.append([flexionValue[0], extensionValue[0], growthScale[0], loaFlexionChk[0], loaExtensionChk[0], inputCrv, zFiber, muscle])
                    else:
                        pass

                if buildLOA:
                    for build in buildLOA:
                        zTool.loaMuscleCustom(*build) # '*' will unpack list

        cmds.select(None)

    def storeSelectedSetup(self):
        fileFilter = '*.ziva'
        savePath = cmds.fileDialog2(fm=0, okc="Save", ff=fileFilter)[0]
        if savePath:
            # Save LOA Curves
            customLOADict = zTool.loaCustomSettings(saveSel=True)
            if customLOADict:
                with open(savePath[:-5]+'_Curves.json', 'w') as f: 
                    json.dump({str(k):v for k, v in customLOADict.items()}, f) # Converts keys from tuples to strings
            # Save Ziva
            z = zva.Ziva()
            z.retrieve_from_scene_selection()
            z.write(savePath)

    def addSuffix(self):
        if cmds.ls(sl=True):
            renameList = cmds.ls(sl=True)
            suffix = self.ui.boneBlendSuffix_Txt.text()
            for item in renameList:
                if item.split('_')[-1]==suffix:
                    cmds.warning('item already has suffix', item)
                else:
                    cmds.rename(item, item+'_'+suffix)

    def removeSuffix(self):
        if cmds.ls(sl=True):
            renameList = cmds.ls(sl=True)
            for item in renameList:
                shortName = item.split('|')[-1:] # returns last item of long name split (short name)
                nameSplit = shortName[0].split('_')[:-1] # removes last item of short name (suffix)
                nameJoin = '_'.join(nameSplit)
                cmds.rename(item, nameJoin)
                zTool.renameNodes()

    def blendBones(self):
        driverBoneSuffix = "_"+self.ui.boneBlendSuffix_Txt.text()
        offVal = self.ui.boneBlendOff_Spn.value()
        onVal = self.ui.boneBlendOn_Spn.value()
        zTool.blendBones(offVal, onVal, driverBoneSuffix)

    def delBlendBones(self):
        driverBoneSuffix = "_"+self.ui.boneBlendSuffix_Txt.text()
        zTool.delBlendBones(driverBoneSuffix)

    def multiObjImport(self):
        importFileList = cmds.fileDialog2(fileMode = 4, fileFilter = "*.*", okc = "Import File(s)")
        zTool.multiObjImport(importFileList)

    def createWrap(self):
        driverBoneSuffix = "_"+self.ui.boneBlendSuffix_Txt.text()
        zTool.createWrap(driverBoneSuffix)

    def keyRunUp(self):
        if cmds.ls(sl=True) == []:
            return IndexError('No selection to key')

        offFrame = self.ui.keyOffFrame_Spn.value()
        onFrame = self.ui.keyOnFrame_Spn.value()
        dampPadding = self.ui.keyDamp_Spn.value()
        itemList = cmds.ls(sl=True)
        zTool.keyRunUp(itemList, offFrame, onFrame, dampPadding)

    def autoFatToMuscle(self, fatObj='fat'):
        '''
        Creates sliding attachments between the fatObj, and all the
        muscles in the muscleLst
        '''
        muscleLst = [u'Mus_L_BicepsFemoris_Part1',
                 u'Mus_L_Biceps_Part2',
                 u'Mus_L_Deltoid_Part3',
                 u'Mus_L_Deltoid_Part1',
                 u'Mus_L_Deltoid_Part2',
                 u'Mus_L_Biceps_Part1',
                 u'Mus_L_ExtensorDigi_Part2',
                 u'Mus_L_ExtensorDigi_Part1',
                 u'Mus_L_ExtensorCarpi_Part1',
                 u'Mus_L_Oblique_Part5',
                 u'Mus_L_Oblique_Part4',
                 u'Mus_L_Oblique_Part3',
                 u'Mus_L_Oblique_Part2',
                 u'Mus_L_Oblique_Part11',
                 u'Mus_L_Oblique_Part10',
                 u'Mus_L_Oblique_Part1',
                 u'Mus_L_Oblique_Part0',
                 u'Mus_L_Gastrocnemius_Part3',
                 u'Mus_L_Gastrocnemius_Part2',
                 u'Mus_L_Gastrocnemius_Part1',
                 u'Mus_L_Flexor_Part3',
                 u'Mus_L_Flexor_Part2',
                 u'Mus_L_Flexor_Part1',
                 u'Mus_L_Fibularislongus_Part1',
                 u'Mus_L_ExtensorLeg_Part1',
                 u'Mus_L_AdductorMagnus_Part1',
                 u'Mus_L_AdductorLongus_Part1',
                 u'Mus_L_VastusLateralis_Part1',
                 u'Mus_L_Triceps_Part2',
                 u'Mus_L_Triceps_Part1',
                 u'Mus_L_Trapezius_Part3',
                 u'Mus_L_Trapezius_Part2',
                 u'Mus_L_Trapezius_Part1',
                 u'Mus_L_Tibialis_Part1',
                 u'Mus_L_TeresMajor_Part1',
                 u'Mus_L_Sterno_Part2',
                 u'Mus_L_Sterno_Part1',
                 u'Mus_L_Semitendinosus_Part1',
                 u'Mus_L_Semimembranosus_Part2',
                 u'Mus_L_Semimembranosus_Part1',
                 u'Mus_L_Satorius_Part1',
                 u'Mus_L_RectusFemoris_Part1',
                 u'Mus_L_PsoasMajor_Part1',
                 u'Mus_L_Pectorial_Part3',
                 u'Mus_L_Pectorial_Part2',
                 u'Mus_L_Pectorial_Part1',
                 u'Mus_L_Pectineus_Part1',
                 u'Mus_L_Oblique_Part9',
                 u'Mus_L_Oblique_Part8',
                 u'Mus_L_Oblique_Part7',
                 u'Mus_L_Oblique_Part6',
                 u'Mus_R_GluteusMedius_Part1',
                 u'Mus_R_Gastrocnemius_Part3',
                 u'Mus_R_Gastrocnemius_Part2',
                 u'Mus_R_Gastrocnemius_Part1',
                 u'Mus_R_Flexor_Part3',
                 u'Mus_R_Flexor_Part2',
                 u'Mus_R_Flexor_Part1',
                 u'Mus_R_Fibularislongus_Part1',
                 u'Mus_R_ExtensorLeg_Part1',
                 u'Mus_R_ExtensorDigi_Part2',
                 u'Mus_R_ExtensorDigi_Part1',
                 u'Mus_R_ExtensorCarpi_Part1',
                 u'Mus_R_Deltoid_Part3',
                 u'Mus_R_Deltoid_Part2',
                 u'Mus_R_Deltoid_Part1',
                 u'Mus_R_BicepsFemoris_Part1',
                 u'Mus_R_Biceps_Part2',
                 u'Mus_R_Biceps_Part1',
                 u'Mus_R_AdductorMagnus_Part1',
                 u'Mus_R_AdductorLongus_Part1',
                 u'Mus_M_Sternohyoid_Part1',
                 u'Mus_M_Abs_Part1',
                 u'Mus_L_VatusMedialis_Part1',
                 u'Mus_R_ExtensorCarpi_Part3',
                 u'Mus_L_ExtensorCarpi_Part3',
                 u'Mus_R_Gluteus_Part1',
                 u'Mus_L_Gluteus_Part1',
                 u'Mus_R_VatusMedialis_Part1',
                 u'Mus_R_VastusLateralis_Part1',
                 u'Mus_R_Triceps_Part2',
                 u'Mus_R_Triceps_Part1',
                 u'Mus_R_Trapezius_Part3',
                 u'Mus_R_Trapezius_Part2',
                 u'Mus_R_Trapezius_Part1',
                 u'Mus_R_Tibialis_Part1',
                 u'Mus_R_TeresMajor_Part1',
                 u'Mus_R_Sterno_Part2',
                 u'Mus_R_Sterno_Part1',
                 u'Mus_R_Semitendinosus_Part1',
                 u'Mus_R_Semimembranosus_Part2',
                 u'Mus_R_Semimembranosus_Part1',
                 u'Mus_R_Satorius_Part1',
                 u'Mus_R_RectusFemoris_Part1',
                 u'Mus_R_PsoasMajor_Part1',
                 u'Mus_R_Pectorial_Part3',
                 u'Mus_R_Pectorial_Part2',
                 u'Mus_R_Pectorial_Part1',
                 u'Mus_R_Pectineus_Part1',
                 u'Mus_R_Oblique_Part9',
                 u'Mus_R_Oblique_Part8',
                 u'Mus_R_Oblique_Part7',
                 u'Mus_R_Oblique_Part6',
                 u'Mus_R_Oblique_Part5',
                 u'Mus_R_Oblique_Part4',
                 u'Mus_R_Oblique_Part3',
                 u'Mus_R_Oblique_Part2',
                 u'Mus_R_Oblique_Part11',
                 u'Mus_R_Oblique_Part10',
                 u'Mus_R_Oblique_Part1',
                 u'Mus_R_Oblique_Part0',
                 u'Mus_R_Latissimus_Part1',
                 u'Mus_R_Infraspinatus_Part2',
                 u'Mus_R_Infraspinatus_Part1',
                 u'Mus_R_Iliacus_Part1',
                 u'Mus_R_Gracilis_Part1',
                 u'Mus_R_GluteusMedius_Part2',
                 u'Mus_L_Latissimus_Part1',
                 u'Mus_L_Infraspinatus_Part2',
                 u'Mus_L_Infraspinatus_Part1',
                 u'Mus_L_Iliacus_Part1',
                 u'Mus_L_Gracilis_Part1',
                 u'Mus_L_GluteusMedius_Part2',
                 u'Mus_L_GluteusMedius_Part1']

        sliding = [u'Mus_L_AdductorLongus_Part1',
                    u'Mus_L_AdductorMagnus_Part1',
                    u'Mus_L_BicepsFemoris_Part1',
                    u'Mus_L_Biceps_Part1',
                    u'Mus_L_Biceps_Part2',
                    u'Mus_L_Deltoid_Part1',
                    u'Mus_L_Deltoid_Part2',
                    u'Mus_L_Deltoid_Part3',
                    u'Mus_L_ExtensorCarpi_Part1',
                    u'Mus_L_ExtensorCarpi_Part3',
                    u'Mus_L_ExtensorDigi_Part1',
                    u'Mus_L_ExtensorDigi_Part2',
                    u'Mus_L_ExtensorLeg_Part1',
                    u'Mus_L_Fibularislongus_Part1',
                    u'Mus_L_Flexor_Part1',
                    u'Mus_L_Flexor_Part2',
                    u'Mus_L_Flexor_Part3',
                    u'Mus_L_Gastrocnemius_Part1',
                    u'Mus_L_Gastrocnemius_Part2',
                    u'Mus_L_Gastrocnemius_Part3',
                    u'Mus_L_GluteusMedius_Part1',
                    u'Mus_L_GluteusMedius_Part2',
                    u'Mus_L_Gluteus_Part1',
                    u'Mus_L_Gracilis_Part1',
                    u'Mus_L_Infraspinatus_Part1',
                    u'Mus_L_Infraspinatus_Part2',
                    u'Mus_L_Latissimus_Part1',
                    u'Mus_L_Oblique_Part1',
                    u'Mus_L_Oblique_Part2',
                    u'Mus_L_Oblique_Part3',
                    u'Mus_L_Oblique_Part4',
                    u'Mus_L_Oblique_Part5',
                    u'Mus_L_Oblique_Part6',
                    u'Mus_L_Oblique_Part7',
                    u'Mus_L_Oblique_Part8',
                    u'Mus_L_Oblique_Part9',
                    u'Mus_L_Oblique_Part10',
                    u'Mus_L_Oblique_Part11',
                    u'Mus_L_Pectorial_Part1',
                    u'Mus_L_Pectorial_Part2',
                    u'Mus_L_Pectorial_Part3',
                    u'Mus_L_PsoasMajor_Part1',
                    u'Mus_L_RectusFemoris_Part1',
                    u'Mus_L_Satorius_Part1',
                    u'Mus_L_Semimembranosus_Part1',
                    u'Mus_L_Semimembranosus_Part2',
                    u'Mus_L_Semitendinosus_Part1',
                    u'Mus_L_Sterno_Part1',
                    u'Mus_L_Sterno_Part2',
                    u'Mus_L_TeresMajor_Part1',
                    u'Mus_L_Tibialis_Part1',
                    u'Mus_L_Trapezius_Part1',
                    u'Mus_L_Trapezius_Part2',
                    u'Mus_L_Trapezius_Part3',
                    u'Mus_L_Triceps_Part1',
                    u'Mus_L_Triceps_Part2',
                    u'Mus_L_VastusLateralis_Part1',
                    u'Mus_L_VatusMedialis_Part1',
                    u'Mus_M_Abs_Part1',
                    u'Mus_M_Sternohyoid_Part1',
                    u'Mus_R_AdductorLongus_Part1',
                    u'Mus_R_AdductorMagnus_Part1',
                    u'Mus_R_BicepsFemoris_Part1',
                    u'Mus_R_Biceps_Part1',
                    u'Mus_R_Biceps_Part2',
                    u'Mus_R_Deltoid_Part1',
                    u'Mus_R_Deltoid_Part2',
                    u'Mus_R_Deltoid_Part3',
                    u'Mus_R_ExtensorCarpi_Part1',
                    u'Mus_R_ExtensorCarpi_Part3',
                    u'Mus_R_ExtensorDigi_Part1',
                    u'Mus_R_ExtensorDigi_Part2',
                    u'Mus_R_ExtensorLeg_Part1',
                    u'Mus_R_Fibularislongus_Part1',
                    u'Mus_R_Flexor_Part1',
                    u'Mus_R_Flexor_Part2',
                    u'Mus_R_Flexor_Part3',
                    u'Mus_R_Gastrocnemius_Part1',
                    u'Mus_R_Gastrocnemius_Part2',
                    u'Mus_R_Gastrocnemius_Part3',
                    u'Mus_R_GluteusMedius_Part1',
                    u'Mus_R_GluteusMedius_Part2',
                    u'Mus_R_Gluteus_Part1',
                    u'Mus_R_Gracilis_Part1',
                    u'Mus_R_Infraspinatus_Part1',
                    u'Mus_R_Infraspinatus_Part2',
                    u'Mus_R_Latissimus_Part1',
                    u'Mus_R_Oblique_Part1',
                    u'Mus_R_Oblique_Part2',
                    u'Mus_R_Oblique_Part3',
                    u'Mus_R_Oblique_Part4',
                    u'Mus_R_Oblique_Part5',
                    u'Mus_R_Oblique_Part6',
                    u'Mus_R_Oblique_Part7',
                    u'Mus_R_Oblique_Part8',
                    u'Mus_R_Oblique_Part9',
                    u'Mus_R_Oblique_Part10',
                    u'Mus_R_Oblique_Part11',
                    u'Mus_R_Pectorial_Part1',
                    u'Mus_R_Pectorial_Part2',
                    u'Mus_R_Pectorial_Part3',
                    u'Mus_R_PsoasMajor_Part1',
                    u'Mus_R_RectusFemoris_Part1',
                    u'Mus_R_Satorius_Part1',
                    u'Mus_R_Semimembranosus_Part1',
                    u'Mus_R_Semimembranosus_Part2',
                    u'Mus_R_Semitendinosus_Part1',
                    u'Mus_R_Sterno_Part1',
                    u'Mus_R_Sterno_Part2',
                    u'Mus_R_TeresMajor_Part1',
                    u'Mus_R_Tibialis_Part1',
                    u'Mus_R_Trapezius_Part1',
                    u'Mus_R_Trapezius_Part2',
                    u'Mus_R_Trapezius_Part3',
                    u'Mus_R_Triceps_Part1',
                    u'Mus_R_Triceps_Part2',
                    u'Mus_R_VastusLateralis_Part1',
                    u'Mus_R_VatusMedialis_Part1']


        # "wrong way". Skin will get stuck on muscles and cannot slide past attchment
        for muscle in muscleLst:
            cmds.select(None)
            cmds.select(fatObj, muscle)
            radius = str(0.10)
            vertList = mm.eval('zFindVerticesByProximity -r ' + radius)
            cmds.select(None)
            if vertList:
                for vert in vertList:
                    cmds.select(vert, add=True)

                cmds.select(muscle, add=True)
                attachNewName = fatObj+"__to__"+muscle+"__zAttachment"
                cmds.refresh()
                allNodes = cmds.ls()
                mm.eval("ziva -a")
                # findAttach = mm.eval('zQuery -t "zAttachment"')[-1]
                newNodes = np.setdiff1d(cmds.ls(), allNodes)
                zAttch = [nde for nde in newNodes if cmds.nodeType(nde)=='zAttachment'][0]
                cmds.setAttr(zAttch+'.attachmentMode', 2)
                cmds.setAttr(zAttch+'.stiffnessExp', 6)
                cmds.rename(str(zAttch), attachNewName)


        '''
        # "correct way" Skin will slide over muscles without getting stuck.
        for muscle in sliding:
            cmds.select(None)
            cmds.select(muscle, fatObj)
            radius = str(0.10)
            vertList = mm.eval('zFindVerticesByProximity -r ' + radius)
            cmds.select(None)
            if vertList:
                for vert in vertList:
                    cmds.select(vert, add=True)

                cmds.select(fatObj, add=True)
                attachNewName = muscle+"__to__"+fatObj+"__zAttachment"
                cmds.refresh()
                allNodes = cmds.ls()
                mm.eval("ziva -a")
                # findAttach = mm.eval('zQuery -t "zAttachment"')[-1]
                newNodes = np.setdiff1d(cmds.ls(), allNodes)
                zAttch = [nde for nde in newNodes if cmds.nodeType(nde)=='zAttachment'][0]
                cmds.setAttr(zAttch+'.attachmentMode', 2)
                cmds.setAttr(zAttch+'.stiffnessExp', 6)
                cmds.rename(str(zAttch), attachNewName)
        '''

if 'zivaWin' in locals():
    zivaWin.hide()
    
zivaWin = ControlMainWindow(parent=maya_main_window())
zivaWin.show()

